//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.

#include "utils.hpp"
#if __cplusplus > 199711L
  // erf is part of <cmath> in c++11
  namespace erfns = std;
#else
  #include <boost/math/special_functions/erf.hpp>
  namespace erfns = boost::math;
#endif
#include <cmath>

using namespace mc;
using namespace std;

mc::scalar mc::normcf(mc::scalar Arg)
{
	return scalar(1)/scalar(2)*(1+erfns::erf(Arg/sqrt(scalar(2))));
}

mc::scalar mc::BSOptionPrice(mc::scalar S0, 
						    mc::scalar K, 
							mc::scalar T, 
							mc::scalar r, 
							mc::scalar sigma,
							bool IsCall)
{
	mc::scalar d1=(log(S0/K)+(r+sigma*sigma/scalar(2))*T)/(sigma*sqrt(T));
	mc::scalar d2=(log(S0/K)+(r-sigma*sigma/scalar(2))*T)/(sigma*sqrt(T));
	if(IsCall)
		return S0*normcf(d1)-exp(-r*T)*K*normcf(d2);
	else
		return exp(-r*T)*K*(1-normcf(d2))-S0*(1-normcf(d1));
}