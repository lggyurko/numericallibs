# README #

This repository contains two c++ libraries: MClib, DOPlib. The code in both library is documented in the source. The MS Visual Studio project is set up for the 2012 version. The project was also tested with gcc 4.8.2.

### DEPENDENCIES ###

* the pre c++11 version requires boost (header only) for shared_ptr, random number generation, some of the vector and matrix containers and linear algebra
* the c++11 version (--makefile=makefileCpp11 or MS Visual Studio 2012) requires boost for 
some of the vector and matrix containers and linear algebra

### CONTENTS ###

* DOPlib - differential operators on R^n. 
* MClib - a Monte-Carlo simulation based numerical library for approximating solutions to stochastic differential equations (SDEs).

### LIMITATIONS ###
Among other issues, the libraries are not thread-safe. 
