//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.

#include "RandomLieSetting.hpp"


using namespace dop;
using namespace dop::aux;


void dop::Degree3_support(Vec_ops & equation, LieSupport & result)
{
	result.clear();
	size_t d=equation.size()-1;

	DVec eta_sup(2);
	eta_sup[0]=DPair(Scalar(-1),Scalar(1.0/2.0));
	eta_sup[1]=DPair(Scalar(1),Scalar(1.0/2.0));

	Vector eta(d);
	size_t e(0);
	Scalar weight(1);
	SupportSetting_Degree3(eta_sup, eta, equation, result, e, weight);

}


void dop::Degree5_support(const Vec_ops & equation, LieSupport & result)
{
	result.clear();

	size_t d=equation.size()-1;
	DVec eta_sup(3), sigma_sup(2);

	eta_sup[0]=DPair(Scalar(0.0),Scalar(2.0/3.0));
	eta_sup[1]=DPair(std::sqrt(Scalar(3.0)),Scalar(1.0/6.0));
	eta_sup[2]=DPair(-std::sqrt(Scalar(3.0)), Scalar(1.0/6.0));

	sigma_sup[0]=DPair(Scalar(1.0),Scalar(1.0/2.0));
	sigma_sup[1]=DPair(Scalar(-1.0),Scalar(1.0/2.0));

	Vector eta(d), sigma(1);

	size_t e(0), s(0);
	Scalar weight(1);
	SupportSetting(eta_sup, sigma_sup, eta, sigma, equation, result,e,s,weight);

}


void dop::Degree5_support_1D(const Vec_ops & equation, LieSupport & result)
{
	result.clear();

	DPair eta0(Scalar(0),Scalar(2.0/3.0));
	DPair eta1(std::sqrt(Scalar(3.0)),Scalar(1.0/6.0));
	DPair eta2(-std::sqrt(Scalar(3.0)), Scalar(1.0/6.0));
	DifferentialOperator temp_do1(equation[0]);
	DifferentialOperator temp_do2(equation[0]);
	DifferentialOperator temp_do3(equation[0]);
	temp_do1+=equation[1]*(eta0.first);
	temp_do2+=equation[1]*(eta1.first);
	temp_do3+=equation[1]*(eta2.first);
	temp_do1+=LieBracket(LieBracket(equation[0],equation[1]),equation[1])*((eta0.first)*(eta0.first)/Scalar(12));
	temp_do2+=LieBracket(LieBracket(equation[0],equation[1]),equation[1])*((eta1.first)*(eta1.first)/Scalar(12));
	temp_do3+=LieBracket(LieBracket(equation[0],equation[1]),equation[1])*((eta2.first)*(eta2.first)/Scalar(12));

	temp_do1.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be
	temp_do2.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be
	temp_do3.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be


	result.push_back(WAlg(temp_do1, eta0.second));
	result.push_back(WAlg(temp_do2, eta1.second));
	result.push_back(WAlg(temp_do3, eta2.second));

}


void dop::Degree5_support_optimized(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								LieSupport & result)
{
	result.clear();
	Matrix points;
	Vector weights;
	SetPoints(points,weights);
	size_t i=0;
	size_t max=points.size1();
	for(; i<max; ++i)
	{
		Vector sigma(1);
		sigma[0]=Scalar(1);
		Matrix_Row points_row(points,i);
		result.push_back(WAlg(Degree5_OneVF(points_row,sigma, equation),weights[i]));

		sigma[0]=Scalar(-1);
		result.push_back(WAlg(Degree5_OneVF(points_row,sigma, equation),weights[i]));
	}
}

void dop::Degree7_support_optimized_1D(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
									   LieSupport & result)
{
	result.clear();
	Matrix points;
	Vector weights;
	SetPoints(points,weights);

	size_t i=0;
	size_t max=points.size1();
	for(; i<max; ++i)
	{
		Vector sigma(1);
		sigma[0]=Scalar(1);
		Matrix_Row points_row(points,i);
		result.push_back(WAlg(Degree7_1D_OneVF(points_row,sigma, equation),weights[i]));

		sigma[0]=Scalar(-1);
		result.push_back(WAlg(Degree7_1D_OneVF(points_row,sigma, equation),weights[i]));
	}

}


void dop::Degree7_support_optimized_2D(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								  LieSupport & result)
{
	result.clear();
	Matrix points;
	Vector weights;

	SetPoints(points,weights);

	size_t i=0;
	size_t max=points.size1();
	for(; i<max; ++i)
	{
		Vector sigma(2);
		sigma[0]=Scalar(1);
		sigma[1]=Scalar(1);
		Matrix_Row points_row(points,i);
		result.push_back(WAlg(Degree7_2D_OneVF(points_row,sigma,equation),weights[i]));

		sigma[0]=Scalar(-1);
		sigma[1]=Scalar(1);
		result.push_back(WAlg(Degree7_2D_OneVF(points_row,sigma,equation),weights[i]));

		sigma[0]=Scalar(1);
		sigma[1]=Scalar(-1);
		result.push_back(WAlg(Degree7_2D_OneVF(points_row,sigma,equation),weights[i]));

		sigma[0]=Scalar(-1);
		sigma[1]=Scalar(-1);
		result.push_back(WAlg(Degree7_2D_OneVF(points_row,sigma,equation),weights[i]));
	 }

}



void dop::aux::SupportSetting_Degree3(const DVec & eta_sup, Vector & eta, Vec_ops & equation, LieSupport & liesup,
							size_t & e, Scalar & weight)
{
	if (e<eta.size())
	{
		++e;
		for(size_t i=0; i<eta_sup.size(); i++)
		{
			eta[e-1]=eta_sup[i].first;
			weight*=eta_sup[i].second;
			SupportSetting_Degree3(eta_sup,eta, equation, liesup,e,weight);
			weight/=eta_sup[i].second;
		}
		--e;
	}
	else if(e>0)
	{
		liesup.push_back(WAlg(Degree3_OneVF(eta, equation),weight));
	}
}

DifferentialOperator dop::aux::Degree3_OneVF(const Vector & eta, Vec_ops & equation)
{
	DifferentialOperator result(equation[0]);
	size_t d=eta.size();

	for(size_t i=0; i<d; ++i)
	{
		result+=equation[i+1]*eta[i];
	}
	result.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be
	return result;
}


void dop::aux::SupportSetting(const DVec & eta_sup, const DVec & sigma_sup,
					 Vector & eta, Vector & sigma, const Vec_ops & equation, LieSupport & liesup,
					 size_t & e, size_t & s, Scalar & weight)
{
	if (e<eta.size())
	{
		++e;
		for(size_t i=0; i<eta_sup.size(); ++i)
		{
			eta[e-1]=eta_sup[i].first;
			weight*=eta_sup[i].second;
			SupportSetting(eta_sup, sigma_sup, eta, sigma, equation, liesup,e,s,weight);
			weight/=eta_sup[i].second;
		}
		--e;
	}
	else if (s<sigma.size() && e>0)
	{
		++s;
		for(size_t i=0; i<sigma_sup.size(); ++i)
		{
			sigma[s-1]=sigma_sup[i].first;
			weight*=sigma_sup[i].second;
			SupportSetting(eta_sup, sigma_sup, eta, sigma, equation, liesup,e,s,weight);
			weight/=sigma_sup[i].second;
		}
		--s;
	}
	else if(e>0)
	{
		liesup.push_back(WAlg(Degree5_OneVF(eta, sigma, equation),weight));
	}
}


DifferentialOperator dop::aux::Degree5_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation)
{
	DifferentialOperator result(equation[0]);
	size_t d(eta.size());

	for(size_t i=0; i<d; ++i)
	{
		size_t k=i+1;
		result+=equation[k]*eta[i];
		result+=LieBracket(LieBracket(equation[0],equation[k]),equation[k])*(eta[i]*eta[i]/Scalar(12));
     }
	//setting cross terms
	size_t k(0);
	for(size_t i=0; i<d; ++i)
	{
		for(size_t j=0; j<i; ++j)
		{
			size_t ii=i+1;
			size_t jj=j+1;
			result+=LieBracket(equation[ii],equation[jj])*(eta[i]*eta[j]*sigma[k]/Scalar(2));
			result+=LieBracket(LieBracket(equation[ii],equation[jj]),equation[jj])*(eta[i]*eta[j]*eta[j]/Scalar(6));
		}
	}
	result.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be
	return result;
}

dop::DifferentialOperator dop::aux::Degree7_1D_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation)
{
	DifferentialOperator result(equation[0]);

	result+=equation[1]*eta[0];
	result+=LieBracket(LieBracket(equation[0],equation[1]),equation[1])*Scalar(1.0/12.0);
	result+=LieBracket(equation[0],equation[1])*(Scalar(1.0/std::sqrt(12.0))*sigma[0]*eta[0]);
	result+=LieBracket(LieBracket(LieBracket(LieBracket(equation[0],equation[1]),equation[1]),equation[1]),equation[1])
		*Scalar(1.0/360.0);

	result.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be
	return result;
}


dop::DifferentialOperator dop::aux::Degree7_2D_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation)
{
	DifferentialOperator result(equation[0]);

	result+=equation[1]*(eta[0]);
	result+=equation[2]*(eta[1]);

	result+= LieBracket( equation[1] , equation[2] )
		*((sigma[1]/std::sqrt(Scalar(6.0))+ eta[0]*eta[1]/Scalar(2.0*std::sqrt(3.0)))* sigma[0]);
	result+= LieBracket( LieBracket( LieBracket( equation[1], equation[2]) , equation[1]), equation[1])
		*(eta[0]*eta[1]*sigma[0]/Scalar(std::sqrt(3.0)*12.0));
	result+= LieBracket( LieBracket( LieBracket( equation[2] , equation[1]), equation[2]), equation[2])
		*(-eta[0]*eta[1]*sigma[0]/Scalar(std::sqrt(3.0)*12.0));
	result+= LieBracket( LieBracket( equation[2] , equation[1]) , equation[1])
		*((Scalar(1.0)+Scalar(2.0)*sigma[1])*eta[1]/Scalar(12.0));
	result+= LieBracket( LieBracket( equation[1] , equation[2]) , equation[2])
		*((Scalar(1.0)+Scalar(2.0)*sigma[1])*eta[0]/Scalar(12.0));
	result+= LieBracket( LieBracket( LieBracket( equation[1] , equation[2] ) , equation[1] ) , LieBracket( equation[1] , equation[2] ) )
		*(-eta[0]/Scalar(90.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[1] , equation[2] ) , equation[1] ) ,equation[1]) ,equation[1] )
		*(-eta[1]/Scalar(4.0*90.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[1] , equation[2]) , equation[1]) , equation[1]) , equation[2])
		*(eta[0]/Scalar(4.0*90.0));
	result+= LieBracket( LieBracket( LieBracket( equation[2] , equation[1]) , equation[2]) , LieBracket( equation[2] , equation[1]) )
		*(-eta[1]/Scalar(90.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[2] , equation[1]) , equation[2]) , equation[2]) , equation[2])
		*(-eta[0]/Scalar(4.0*90.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[2] , equation[1]) , equation[2]) , equation[2]) , equation[1])
		*(eta[1]/Scalar(4.0*90.0));
	result+= LieBracket( LieBracket( equation[0] , equation[1]) , equation[1])
		*Scalar(1.0/12.0);
	result+= LieBracket( LieBracket( equation[0] , equation[2]) , equation[2])
		*Scalar(1.0/12.0);
	result+= LieBracket( equation[0] , equation[1])
		*(-eta[0]*sigma[0]/Scalar(std::sqrt(12.0)));
	result+= LieBracket( equation[0] , equation[2])
		*(eta[1]*sigma[0]/Scalar(std::sqrt(12.0)));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[0] , equation[1]) , equation[1]) , equation[1]) , equation[1])
		*Scalar(1.0/(45.0*8.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[0] , equation[2]) , equation[2]) , equation[2]) , equation[2])
		*Scalar(1.0/(45.0*8.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[0] , equation[2]) , equation[1]) , equation[1]) , equation[2])
		*Scalar(1.0/(45.0*8.0));
	result+= LieBracket( LieBracket( LieBracket( LieBracket( equation[0] , equation[1]) , equation[1]) , equation[2]) , equation[2])
		*Scalar(1.0/(45.0*8.0));
	result+= LieBracket( LieBracket( LieBracket( equation[1] , equation[2]) , equation[2] ) , LieBracket( equation[0] , equation[1] ) )
		*Scalar(-1.0/(45.0*8.0));
	result+= LieBracket( LieBracket( LieBracket( equation[1] , equation[2]) , equation[1] ) , LieBracket( equation[0] , equation[2] ) )
		*Scalar(1.0/(45.0*8.0));
	result+= LieBracket( LieBracket( LieBracket( equation[0] , equation[2]) , equation[1] ) , LieBracket( equation[1] , equation[2] ) )
		*Scalar(-1.0/(15.0*4.0));
	result+= LieBracket( LieBracket( LieBracket( equation[0] , equation[1]) , equation[2] ) , LieBracket( equation[1] , equation[2] ) )
		*Scalar(1.0/90.0);

	result.TruncateByLength(size_t(1));//ideally, there is no need for this line, but practically might be

	return result;
}


void dop::aux::GaussianCubature_Degree5_Dim2_size7(Matrix & points, Vector & weights)
{
	points.resize(7,2);
	weights.resize(7);
	Scalar r(2.0), s(1.0), t(std::sqrt(3.0));
	Scalar B=Scalar(1.0/24.0);
	Scalar A=Scalar(1.0/4.0);

	points(0,0)=Scalar(0);
	points(0,1)=Scalar(0);
	weights[0]=A;

	points(1,0)=r;
	points(1,1)=Scalar(0);
	weights[1]=B;

	points(2,0)=-r;
	points(2,1)=Scalar(0);
	weights[2]=B;

	points(3,0)=s;
	points(3,1)=t;
	weights[3]=B;

	points(4,0)=-s;
	points(4,1)=t;
	weights[4]=B;

	points(5,0)=s;
	points(5,1)=-t;
	weights[5]=B;

	points(6,0)=-s;
	points(6,1)=-t;
	weights[6]=B;
}


void dop::aux::GaussianCubature_Degree5_Dim4_size25(Matrix & points, Vector & weights)
{
	points.resize(25,4);
	weights.resize(25);
	//scalar r(rational(0),rational(1), 3);
	Scalar r(std::sqrt(3.0));
	Scalar B(Scalar(1.0/72.0));
	size_t index(0);
	for(size_t i=0; i<3; ++i)
	{
		for(size_t j=i+1; j<4; ++j)
		{
			Vector temp(4);
			temp*=Scalar(0);
			temp[i]=r;
			temp[j]=r;
			Scalar ii(Scalar(-1));
			for(int i1=0; i1<2; i1++)
			{
				temp[i]*=ii;
				for(int i2=0; i2<2; i2++)
				{
					temp[j]*=ii;
					Matrix_Row points_row(points,index);
					points_row=temp;
					weights[index]=B;
					++index;
				}
			}
		}
	}
	Matrix_Row points_row(points,index);
	points_row*=Scalar(0);
	weights[index]=Scalar(1.0/6.0);
}

void dop::aux::GaussianCubature_Degree7_Dim1_size4(Matrix & points, Vector & weights)
{
	points.resize(4,1);
	weights.resize(4);
	Scalar r(std::sqrt(6.0));
	Scalar A(1.0/(4.0*(3-r)));
	Scalar B(1.0/(4.0*(3+r)));
    Scalar normalise(2.0*A + 2.0*B);
	A /= (normalise*2.0);
    B /= (normalise*2.0);

	points(0,0)=Scalar(std::sqrt(Scalar(3)-r));
	weights[0]=A;

	points(1,0)=Scalar(-std::sqrt(Scalar(3)-r));
	weights[1]=A;

	points(2,0)=Scalar(std::sqrt(Scalar(3)+r));
	weights[2]=B;

	points(3,0)=Scalar(-std::sqrt(Scalar(3)+r));
	weights[3]=B;

}

void dop::aux::GaussianCubature_Degree7_Dim2_size12(Matrix & points, Vector & weights)
{
	points.resize(12,2);
	weights.resize(12);
	Scalar r(std::sqrt(6.0));
	Scalar s(std::sqrt((9.0-3.0*std::sqrt(5.0))/4.0));
	Scalar cubT(std::sqrt((9.0+3.0*std::sqrt(5.0))/4.0));
	Scalar A(1.0/36.0);
	Scalar B((5.0+2.0*std::sqrt(5.0))/45.0);
	Scalar C((5.0-2.0*std::sqrt(5.0))/45.0);

	A /= Scalar(4);
    B /= Scalar(4);
	C /= Scalar(4);

	points(0,0)=r;
	points(0,1)=Scalar(0);
	weights(0)=A;

	points(1,0)=Scalar(0);
	points(1,1)=r;
	weights(1)=A;

	points(2,0)=-r;
	points(2,1)=Scalar(0);
	weights(2)=A;

	points(3,0)=Scalar(0);
	points(3,1)=-r;
	weights(3)=A;

	points(4,0)=s;
	points(4,1)=s;
	weights(4)=B;

	points(5,0)=-s;
	points(5,1)=s;
	weights(5)=B;

	points(6,0)=s;
	points(6,1)=-s;
	weights(6)=B;

	points(7,0)=-s;
	points(7,1)=-s;
	weights(7)=B;

	points(8,0)=cubT;
	points(8,1)=cubT;
	weights(8)=C;

	points(9,0)=cubT;
	points(9,1)=-cubT;
	weights(9)=C;

	points(10,0)=-cubT;
	points(10,1)=cubT;
	weights(10)=C;

	points(11,0)=-cubT;
	points(11,1)=-cubT;
	weights(11)=C;

}

