//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once

#ifndef __IFUNCTION_H__
#define __IFUNCTION_H__

#include "CommonTypedefs.hpp"
#if __cplusplus > 199711L  
  #include <memory>
  #include <cstddef>
  namespace sptr = std;
#else
  #include <boost/shared_ptr.hpp>
  #include <boost/enable_shared_from_this.hpp>
  namespace sptr = boost; 
#endif
#include <map>
#include <set>
#include <iostream>






namespace dop
{




class IFunction;
class IMonomialComposite;
class ISumComposite;
class IFunctionComposite;
typedef sptr::shared_ptr<IFunction> IFunction_ptr;


//**************************************************************
//**************************************************************
// ORDERING
//**************************************************************
//**************************************************************

namespace aux
{
/** \class SumCompLess
	\brief Function object for ordering IFunction_ptr's when stored in a SumSet
*/
	class SumCompLess : public std::binary_function<const dop::IFunction_ptr,const dop::IFunction_ptr,bool>
{
public:
	bool operator()(const dop::IFunction_ptr & arg1,const dop::IFunction_ptr & arg2) const;
};

/** \class MonomialCompare
	\brief Auxiliary Function object used in SumCompLess for ordering IMonomialComposite instances
*/
class MonomialCompare : public std::binary_function<const dop::IFunction_ptr,const dop::IFunction_ptr,bool>
{
public:
	bool operator()(const dop::IFunction_ptr & arg1,const dop::IFunction_ptr & arg2) const;
};

/** \class MonomialCompLess
	\brief Function object for ordering IFunction_ptr's when stored in a MonomialMap
*/
class MonomialCompLess : public std::binary_function<const dop::IFunction_ptr,const dop::IFunction_ptr,bool>
{
public:
	bool operator()(const dop::IFunction_ptr & arg1,const dop::IFunction_ptr & arg2) const;
};

/** \class PtrLess
	\brief Function object for general ordering IFunction_ptr based on memory address
*/
template<typename A>
class PtrLess : public std::binary_function<const A,const A,bool>
{
public:
	bool operator()(const A & arg1,const A & arg2) const
	{
		return arg1.get()<arg2.get();
	}
};


/** \class SingleDiffMap
	\brief A Singleton kind object to map leaf IFunction's to their partial deivatives
*/
class SingleDiffMap
{
	typedef std::map<dop::DiffIndex,dop::IFunction_ptr> IndexIFunMap;
	typedef std::pair<dop::DiffIndex,dop::IFunction_ptr> IndexIFunPair;
	typedef std::map<dop::IFunction_ptr, IndexIFunMap, PtrLess<dop::IFunction_ptr> > DiffMap;
	typedef std::pair<dop::IFunction_ptr, IndexIFunMap> DiffPair;

public:
	static void Insert(dop::IFunction_ptr & arg1, dop::DiffIndex index, dop::IFunction_ptr & arg2);
	static IFunction_ptr Find(dop::IFunction_ptr & arg1, dop::DiffIndex index);
private:
	SingleDiffMap();
	SingleDiffMap(const SingleDiffMap &);
	SingleDiffMap & operator=(const SingleDiffMap &);

	static DiffMap& Instance()
	{
		static DiffMap obj;
		return obj;
	}

};
}

//**************************************************************
//**************************************************************
// ISUMCOMPOSITE
//**************************************************************
//**************************************************************

/*! \class IFunction
	\brief Base class to implement the behavior of the Function class.

	Function is implemented using the bridge pattern. The actual behavior
	is hidden behind a smart pointer to an IFunction.
	IFunction is an abstract class, defining the interface and in some cases
	the default behavior. Particular derived classes are the ISumComposite and
	IMonomialComposite, also ICoordinate.
	\todo IFunctionComposite is yet to be written. In particular: Clone(), Diff().
*/
class IFunction : public sptr::enable_shared_from_this<IFunction>
{
public:
	/**
	* Virtual destructor.
	*/
	virtual ~IFunction(){}

	/**
	* The operator () defines the key functionality.
	*/
	virtual Scalar operator()(const Vector &) const=0;

	/**
	* Testing if the derived class is ISumComposite.
	* @return A bool and a pointer to ISumComposite, which is NULL if it's not the type.
	*/
	virtual std::pair<bool, ISumComposite *> isSc() {return std::pair<bool,ISumComposite *>(false,nullptr_t());}

	/**
	* Testing if the derived class is IMonomialComposite.
	* @return A bool and a pointer to IMonomialComposite, which is NULL if it's not the type.
	*/
	virtual std::pair<bool, IMonomialComposite *> isMc() {return std::pair<bool,IMonomialComposite *>(false,nullptr_t());}

	/**
	* Testing if the derived class is IFunctionComposite.
	* @return A bool and a pointer to IFunctionComposite, which is NULL if it's not the type.
	*/
	virtual std::pair<bool, IFunctionComposite *> isFc() {return std::pair<bool,IFunctionComposite *>(false,nullptr_t());}

	/**
	* Testing if the derived class is a Scalar function.
	* Several IFunction instances could be scalar, however the returned object is of the form of ISumComposite.
	* @return A bool and a pointer to ISumComposite, which is NULL if it's not the type.
	*/
	virtual std::pair<bool, ISumComposite *> isScalar() {return std::pair<bool,ISumComposite *>(false,nullptr_t());}

	/**
	* Testing if the derived class is a zero function.
	*/
	virtual bool isZero() const {return false;}

	/**
	* Cloning the instance.
	* Does not create copies of the leafs, however copies the structure of the non-leaf composites.
	*/
	virtual IFunction_ptr Clone()  {return shared_from_this();}

	/**
	* Partial differentiation.
	* If leaf, looks up the partial differential in the SingleDiffMap container. If non-leaf
	* differentiates the structure, using the differentiation rules implemented in the particular
	* derived classes (composites).
	* @param arg is the differentiation index.
	*/
	virtual IFunction_ptr Diff(DiffIndex arg)
	{
	    IFunction_ptr temp_ftr(shared_from_this());
		return aux::SingleDiffMap::Find(temp_ftr,arg);
	}

	/**
	* Outputing the function.
	* Auxiliary member, mainly for testing purposes.
	*/
	virtual std::ostream&  Print(std::ostream & os) const=0;

};

IFunction_ptr operator+(const IFunction_ptr &, const IFunction_ptr & );
IFunction_ptr operator*(const IFunction_ptr &, const IFunction_ptr & );
IFunction_ptr operator+(const Scalar, const IFunction_ptr & );
IFunction_ptr operator*(const Scalar, const IFunction_ptr & );
IFunction_ptr operator+(const IFunction_ptr &, const Scalar);
IFunction_ptr operator*(const IFunction_ptr &, const Scalar);

/*! \fn IFunction_ptr pow(const IFunction_ptr & IFptr, const Exponent exparg)
	\brief Taking the power of an IFunction.
	\param IFptr the base
	\param exparg the exponent

	If IFptr is not Scalar, the function returns an IFunction_ptr to an IMonomialComposite, so the differentiation
	is taken care of.
*/
IFunction_ptr pow(const IFunction_ptr &, const Exponent);


//**************************************************************
//**************************************************************
// ISUMCOMPOSITE
//**************************************************************
//**************************************************************

/*! \class ISumComposite
	\brief Derived class from IFunction, used for implementing additive composition.

	ISumComposite models the function objects of the form: scalar + sum_i a_i, where
	each a_i is an IFunction. The sum is stored in a set, and the scalar in a Scalar.
	ISumComposite is Scalar, when the set is empty.
	Crucial overridden members: operator(), Clone, Diff, isSc, isScalar, isZero.
*/
class ISumComposite : public IFunction
{
	friend IFunction_ptr dop::pow(const IFunction_ptr & arg1, const Exponent arg2);
public:

	/**
	* Typedef, SumSet.
	* SumCompLess compares based on the memory address, except the IMonomialComposites, which
	* are ordered in a lexicographical kind of way.
	*/
	typedef std::set<IFunction_ptr, aux::SumCompLess > SumSet;
public:
	/**
	* Default constructor.
	* Returns Scalar ISumComposite.
	*/
	ISumComposite(Scalar arg=Scalar(0));

	/**
	* Copy constructor.
	* The argument is cloned.
	*/
	ISumComposite(const ISumComposite &);

	/**
	* Copy assignment.
	* The argument is cloned.
	*/
	ISumComposite & operator=(const ISumComposite &);

	/**
	* The overriden operator().
	* @return addConst_+ sum of operator() of each element in the SumSet sum_.
	* @see IFunction
	*/
	Scalar operator()(const Vector &) const;

	ISumComposite & operator +=(const IFunction_ptr & arg);
	ISumComposite & operator +=(const Scalar);
	ISumComposite & operator *=(const IFunction_ptr & arg);
	ISumComposite & operator *=(const Scalar);

	/**
	* The overriden Clone.
	* Clones the structure, i.e. the SumSet member, but not the pointed IFunctions in the container.
	* @see IFunction
	*/
	IFunction_ptr Clone();

	/**
	* The overriden Diff.
	* Uses the differentiation rule of the sum. (I.e. termwise)
	* @see IFunction
	*/
	IFunction_ptr Diff(DiffIndex arg);

	/**
	* The overriden isSc.
	* @see IFunction
	*/
	std::pair<bool, ISumComposite *> isSc();

	/**
	* The overriden isScalar.
	* Scalar if the SumSet member is empty.
	* @see IFunction
	*/
	std::pair<bool, ISumComposite *> isScalar();

	/**
	* The overriden isZero.
	* Zero if the SumSet member is empty and the additive constant is zero.
	* @see IFunction
	*/
	bool isZero() const;

	/**
	* Outputing the function.
	* Auxiliary member, mainly for testing purposes.
	*/
	std::ostream&  Print(std::ostream & os) const;

	/**
	* Get function, returning the additive constant.
	*/
	Scalar GetAddConst() const {return addConst_;}

	/** Checks if the sum is actually a single term IFunction_ptr
	*/
	std::pair<bool,IFunction_ptr> isSingleTerm();

private:

	SumSet sum_; //!< SumSet container, containing terms of the sum
	Scalar addConst_; //!< Additive constant
};

//**************************************************************
//**************************************************************
// IMONOMIALCOMPOSITE
//**************************************************************
//**************************************************************

/*! \class IMonomialComposite
	\brief Derived class from IFunction, used for implementing multiplicative composition.

	IMonomialComposite models the function objects of the form: scalar * prod_i a_i^bi, where
	each a_i is an IFunction. The product is stored in a map container monomial_, mapping IFunction_ptr's (a_i)
	to exponents (b_i). A scalar multiplier is stored in the Scalar multConst_.
	Crucial overridden members: operator(), Clone, Diff, isSc, isScalar, isZero.
*/
class IMonomialComposite : public IFunction
{
	friend class aux::MonomialCompLess;
	friend class aux::MonomialCompare;
	friend class ISumComposite;
public:
	typedef std::map<IFunction_ptr, Exponent, aux::MonomialCompLess > MonomialMap;
	typedef std::pair<IFunction_ptr, Exponent > MonomialPair;
public:
	/**
	* Default constructor.
	* Constructs an empty monomial_ and sets mulConst_=arg.
	*/
	IMonomialComposite(Scalar arg=Scalar(1));

	/**
	* Constructing the power of an IFunction_ptr.
	* Constructs Iarg^Earg
	*/
	IMonomialComposite(const IFunction_ptr & Iarg, Exponent Earg=Exponent(1));

	/**
	* Constructing the power of an IMonomialComposite.
	* Constructs arg^Earg
	*/
	IMonomialComposite(const IMonomialComposite & arg,Exponent Earg=Exponent(1));

	/**
	* Copy assignment
	*/
	IMonomialComposite & operator=(const IMonomialComposite & arg);


	/**
	* The overriden operator().
	* @return multConst_* prod of operator() of each element in the MonomialMap monomial_
	* @see IFunction
	*/
	Scalar operator()(const Vector &) const;

	IMonomialComposite & operator*=(const Scalar);
	IMonomialComposite & operator*=(const IFunction_ptr & arg);

	/**
	* The overriden Clone.
	* Clones the structure, i.e. the MonomialMap member, but not the pointed IFunctions in the container.
	* @see IFunction
	*/
	IFunction_ptr Clone() ;

	/**
	* The overriden Diff.
	* Uses the differentiation rule of the power and product.
	* @see IFunction
	*/
	IFunction_ptr Diff(DiffIndex arg);

	/**
	* The overriden isMC.
	* @see IFunction
	*/
	std::pair<bool, IMonomialComposite *> isMc() {	return std::pair<bool, IMonomialComposite *>(true,this);}

	/**
	* The overriden isScalar.
	* Scalar if the MonomialMap member is empty. If scalar, a new ISumComposite instance is constructed.
	* This must be deleted manualy.
	* @see IFunction
	\todo Consider returning smart (from this) pointers instead of raw pointers.
	*/
	std::pair<bool, ISumComposite *> isScalar() ;

	/**
	* The overriden isZero.
	* Zero if the multiplicative constant is zero.
	* @see IFunction
	*/
	bool isZero() const;

	/**
	* Outputing the function.
	* Auxiliary member, mainly for testing purposes.
	*/
	std::ostream&  Print(std::ostream & os) const;

	/**
	*	Takes the Earg'th power of this.
	*/
	IMonomialComposite & pow(Exponent Earg);
private:

	MonomialMap monomial_; //!< MonomialMap container, containing terms of the product
	Scalar multConst_; //!< Multiplicative constant
};


}



#endif //__IFUNCTION_H__
