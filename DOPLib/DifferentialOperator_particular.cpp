//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.

#include "DifferentialOperator_particular.hpp"

using namespace dop;
#include <cmath>


Vec_ops dop::GeometricBM1D(Scalar mu, Scalar sigma, Degree maxDegree, bool isStrat)
{
	// dX=mu X dt + sigma X dB
	Vec_ops result;
	//drift
	DifferentialOperator V0(size_t(1), maxDegree,
					Degree(1), DiffIndex(0), Function(size_t(0))*mu);
	//vol
	DifferentialOperator V1(size_t(1), maxDegree,
					Degree(0.5), DiffIndex(0), Function(size_t(0))*sigma);
	//Stratonovich correction
	if(isStrat)
		V0-=0.5*(V1*V1).TruncateByLength(size_t(1));

	result.push_back(V0);
	result.push_back(V1);
	return result;
}
Vec_ops dop::GeometricBMnAverage1D(Scalar mu, Scalar sigma, Degree maxDegree, bool isStrat)
{
	// dX=mu X dt + sigma X dB.
	// dA=X dt.
	Vec_ops result;
	//drift
	DifferentialOperator V0(size_t(2), maxDegree);
	V0.Insert(Degree(1), DiffIndex(0), Function(size_t(0))*mu);
	V0.Insert(Degree(1),DiffIndex(1),Function(size_t(0)));

	//vol
	DifferentialOperator V1(size_t(2), maxDegree);
	V1.Insert(Degree(0.5), DiffIndex(0), Function(size_t(0))*sigma);
	//Stratonovich correction
	if(isStrat)
		V0-=0.5*(V1*V1).TruncateByLength(size_t(1));

	result.push_back(V0);
	result.push_back(V1);
	return result;

}
Vec_ops dop::GeometricBM2D(Scalar mu1, Scalar sigma1, Scalar mu2,
					  Scalar sigma2, Scalar rho, Degree maxDegree, bool isStrat)
{
	// dX1=mu1 X1 dt + sigma1 X1 dB1.
	// dX2=mu2 X2 dt + sigma2 rho X2 dB1 + sigma2 sqrt(1-rho^2) X2 dB2

	Vec_ops result;
	//drift
	DifferentialOperator V0(size_t(2), maxDegree,
					Degree(1), DiffIndex(0), Function(size_t(0))*mu1);
	V0.Insert(Degree(1),DiffIndex(1),Function(size_t(1))*mu2);

	//vol1
	DifferentialOperator V1(size_t(2), maxDegree,
					Degree(0.5), DiffIndex(0), Function(size_t(0))*sigma1);
	V1.Insert(Degree(0.5),DiffIndex(1),Function(size_t(1))*(sigma2*rho));

	//vol2
	DifferentialOperator V2(size_t(2), maxDegree,
			Degree(0.5), DiffIndex(1), Function(size_t(1))*(sigma2*std::sqrt(1-rho*rho)));

	//Stratonovich correction
	if(isStrat)
	{
		V0-=0.5*(V1*V1).TruncateByLength(size_t(1));
		V0-=0.5*(V2*V2).TruncateByLength(size_t(1));
	}

	result.push_back(V0);
	result.push_back(V1);
	result.push_back(V2);
	return result;

}
Vec_ops dop::StochasticCEVVol1D(Scalar mu, Scalar sigma, Scalar a, Scalar b,
						   Scalar c, Scalar rho, Exponent e, Degree maxDegree, bool isStrat)
{
  // dX=mu X dt + sigma sqrt(V) X dB1.
  // dV=a(b-V)dt + c V^e(rho dB1+sqrt(1-rho^2) dB2)
	Vec_ops result;
	//drift
	DifferentialOperator V0(size_t(2), maxDegree,
					Degree(1), DiffIndex(0), Function(size_t(0))*mu);
	V0.Insert(Degree(1),DiffIndex(1),a*b-a*Function(size_t(1)));

	//vol1
	DifferentialOperator V1(size_t(2), maxDegree,
					Degree(0.5), DiffIndex(0), Function(size_t(0))*Function(size_t(1),e)*sigma);
	V1.Insert(Degree(0.5),DiffIndex(1),Function(size_t(1),e)*(c*rho));

	//vol2
	DifferentialOperator V2(size_t(2), maxDegree,
			Degree(0.5), DiffIndex(1), Function(size_t(1),e)*(c*std::sqrt(1-rho*rho)));

	//Stratonovich correction
	if(isStrat)
	{
		V0-=0.5*(V1*V1).TruncateByLength(size_t(1));
		V0-=0.5*(V2*V2).TruncateByLength(size_t(1));
	}

	result.push_back(V0);
	result.push_back(V1);
	result.push_back(V2);
	return result;

}


Vec_ops dop::StochasticCEVVolnAverage1D(Scalar mu, Scalar sigma, Scalar a, Scalar b,
						   Scalar c, Scalar rho, Exponent e, Degree maxDegree, bool isStrat)

{
  // dX=mu X dt + sigma sqrt(V) X dB1.
  // dA=Xdt
  // dV=a(b-V)dt + c V^e(rho dB1+sqrt(1-rho^2) dB2)
	Vec_ops result;
	//drift
	DifferentialOperator V0(size_t(3), maxDegree);
	V0.Insert(Degree(1),DiffIndex(0),Function(size_t(0))*Function(size_t(2),e)*mu);
	V0.Insert(Degree(1),DiffIndex(1),Function(size_t(0)));
	V0.Insert(Degree(1),DiffIndex(2),Function(size_t(2))*(-a)+a*b);

	//vol1
	DifferentialOperator V1(size_t(3), maxDegree);
	V1.Insert(Degree(0.5),DiffIndex(0),Function(size_t(0))*sigma);
	V1.Insert(Degree(0.5),DiffIndex(2),Function(size_t(2),e)*(c*rho));

	//vol2
	DifferentialOperator V2(size_t(3), maxDegree);
	V2.Insert(Degree(0.5), DiffIndex(2), Function(size_t(2),e)*(c*std::sqrt(1-rho*rho)));

	//Stratonovich correction
	if(isStrat)
	{
		V0-=0.5*(V1*V1).TruncateByLength(size_t(1));
		V0-=0.5*(V2*V2).TruncateByLength(size_t(1));
	}

	result.push_back(V0);
	result.push_back(V1);
	result.push_back(V2);
	return result;

}
