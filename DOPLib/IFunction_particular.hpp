//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once

#ifndef __IFUNCTION_PARTICULAR_H__
#define __IFUNCTION_PARTICULAR_H__

#include <map>
#include "IFunction.hpp"

namespace dop
{

/*! \class ICoordinate
	\brief Particular class derived form IFunction.

	Model of coordinate function. The operator() returns
	a particular coordinate of the argument. If constracted through
	the static member function Instance, each coordinate function is
	constructed only once.
*/
class ICoordinate : public IFunction
{
public:

	/** Constructor.
	*/
	ICoordinate(size_t index) : index_(index) {}

	/**
	* The overriden operator().
	* @return the coordinate of arg corresponding to index_
	* @see IFunction
	*/
	Scalar operator()(const Vector & arg) const
	{
		if(arg.size()<=index_)
			return Scalar(0);
		else
			return arg(index_);
	}

	/** Instance constructs and stores coordinate functions in a static container.
	*	Instances are stored in a map<size_t,IFunction_ptr>
	*/
	static IFunction_ptr Instance(size_t index)
	{
		static std::map<size_t,IFunction_ptr> objMap;
		std::map<size_t,IFunction_ptr>::iterator iter=objMap.find(index);
		if(iter==objMap.end())
		{
			std::pair<std::map<size_t,IFunction_ptr>::iterator,bool> testInsert=
				objMap.insert(std::pair<size_t,IFunction_ptr>(index,IFunction_ptr(new ICoordinate(index))));
			iter=testInsert.first;
		}
		return (*iter).second;
	}

	/**
	* Outputing the function.
	* Auxiliary member, mainly for testing purposes.
	*/
	std::ostream&  Print(std::ostream & os) const
	{
		os<< " ICoordinate" << index_ << " ";
		return os;
	}

private:
	size_t index_; //!< Contains to coordinate index.

};





}






#endif //__IFUNCTION_PARTICULAR_H__


