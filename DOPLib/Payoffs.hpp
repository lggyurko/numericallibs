//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved. 
#pragma once

#ifndef _PAYOFFS_H__
#define _PAYOFFS_H__

namespace dop
{

/** \class TerminalCond_EuropeanCall
	\brief Implementation of a particular payoff function.
	*/
class TC_EuropeanCall 
{
public:
	/** Constructor
	  * @param K strike
	  */
	TC_EuropeanCall(Scalar K=Scalar(0)) : K_(K), dim_(1) {}

	/** The actual functionality.
	  * @param vecIn point to evaluate terminal condition at
	  * @param vecOut result written into 
	  */
	void operator()(const Vector & vecIn, Vector & vecOut)
	{
		vecOut[0]=std::max<Scalar>(vecIn[0]-K_,Scalar(0));
	}

	size_t GetDim() const {return dim_;}
private:
	Scalar K_; //!< strike 
	const size_t dim_;
};

/** \class TC_VariousCalls
	\brief Implementation of six particular payoff functions.

	Created for the model StochasticCEVVolnAverage1D.
	*/
class TC_VariousCalls
{
public:
	/** Constructor
	  * @param K strike
	  * @param T time to maturity
	  */
	TC_VariousCalls(Scalar T, Scalar K=Scalar(0)) : K_(K), T_(T), dim_(6){}

	/** The actual functionality.
	  * @param vecIn point to evaluate terminal condition at
	  * @param vecOut result written into 
	  */
	void operator()(const Vector & vecIn, Vector & vecOut)
	{
		vecOut[0]=vecIn[0]; //E(Share_T)
		vecOut[1]=vecIn[1]; //E(A)
		vecOut[2]=vecIn[2];	//E(vol_T)
		vecOut[3]=std::max<Scalar>(vecIn[0]-K_,Scalar(0)); //European call
		vecOut[4]=std::max<Scalar>(vecIn[0]-vecIn[1]/T_,Scalar(0));//Asian call1
		vecOut[5]=std::max<Scalar>(vecIn[1]/T_-K_*0.5,Scalar(0));//Asian call2
	}

	size_t GetDim() const {return dim_;}
private:
	Scalar K_; //!< strike 
	Scalar T_; //!< time to maturity
	const size_t dim_; //!< dimension of result
};


/** \class TC_VariousCalls
	\brief Implementation of six particular payoff functions.

	Created for the model GeometricBM2D.
	*/
class TC_VariousCalls2
{
public:
	/** Constructor
	  * @param K strike
	  * @param T time to maturity
	  */
	TC_VariousCalls2(Scalar T, Scalar K=Scalar(0)) : K_(K), T_(T), dim_(6){}

	/** The actual functionality.
	  * @param vecIn point to evaluate terminal condition at
	  * @param vecOut result written into 
	  */
	void operator()(const Vector & vecIn, Vector & vecOut)
	{
		vecOut[0]=vecIn[0]; //E(Share_T)
		vecOut[1]=vecIn[1]; //E(A)
		vecOut[2]=std::max<Scalar>(vecIn[0]-K_,Scalar(0)); //European call
		vecOut[3]=std::max<Scalar>(vecIn[0]-1.5*K_,Scalar(0)); //European call
		vecOut[4]=std::max<Scalar>(vecIn[1]-K_,Scalar(0)); //European call
		vecOut[5]=std::max<Scalar>(vecIn[1]-1.5*K_,Scalar(0)); //European call
		
	}

	size_t GetDim() const {return dim_;}
private:
	Scalar K_; //!< strike 
	Scalar T_; //!< time to maturity
	const size_t dim_; //!< dimension of result
};




/** \class TC_Asian1D
	\brief Implementation of five particular payoff functions.

	Created for the model GeometricBMnAverage1D.
	*/
class TC_Asian1D
{
public:
	/** Constructor
	  * @param K strike
	  * @param T time to maturity
	  */
	TC_Asian1D(Scalar T, Scalar K=Scalar(0)) : K_(K), T_(T), dim_(5){}

	/** The actual functionality.
	  * @param vecIn point to evaluate terminal condition at
	  * @param vecOut result written into 
	  */
	void operator()(const Vector & vecIn, Vector & vecOut)
	{
		vecOut[0]=vecIn[0]; //E(Share_T)
		vecOut[1]=vecIn[1]; //E(A)
		vecOut[2]=std::max<Scalar>(vecIn[0]-K_,Scalar(0)); //European call
		vecOut[3]=std::max<Scalar>(vecIn[0]-vecIn[1]/T_,Scalar(0));//Asian call1
		vecOut[4]=std::max<Scalar>(vecIn[1]/T_-K_*0.5,Scalar(0));//Asian call2
	}

	size_t GetDim() const {return dim_;}
private:
	Scalar K_; //!< strike 
	Scalar T_; //!< time to maturity
	const size_t dim_; //!< dimension of result
};


}
#endif //_PAYOFFS_H__