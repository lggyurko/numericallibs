//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once

#ifndef _RANDOMLIESETTING_H__
#define _RANDOMLIESETTING_H__

#include <utility>
#include <map>
#include <vector>
#include <cmath>
#include "DifferentialOperator.hpp"
#include "CommonTypedefs.hpp"
#include <boost/numeric/ublas/matrix_proxy.hpp>

namespace dop
{

typedef boost::numeric::ublas::matrix_row<Matrix> Matrix_Row;
typedef std::pair<DifferentialOperator, Scalar> WAlg;
typedef std::vector<WAlg> LieSupport;
typedef std::pair<Scalar,Scalar> DPair;
typedef std::vector<DPair> DVec;
typedef std::vector<DifferentialOperator> Vec_ops;



/** \fn void Degree5_support(const Vec_ops & equation, LieSupport & result)
	\brief Setting up a degree 3 cubature support with 2^d elements.
	\param equation set of coefficient functions.
	\param result the representation of the vector field valued random variable written into

	Based on the non-optimized Gaussian cubature.
*/
void Degree3_support(Vec_ops & equation, LieSupport & result);


/** \fn void Degree5_support(const Vec_ops & equation, LieSupport & result)
	\brief Setting up a degree 5 cubature support.
	\param equation set of coefficient functions.
	\param result the representation of the vector field valued random variable written into

	Based on the non-optimized Gaussian cubature.
*/
void Degree5_support(const Vec_ops & equation, LieSupport & result);



/** \fn void Degree5_support_1D(const Vec_ops & equation, LieSupport & result);
	\brief Setting up a degree 5 cubature support, specialized for 1D equations.
	\param equation set of coefficient functions.
	\param result the representation of the vector field valued random variable written into
*/
void Degree5_support_1D(const Vec_ops & equation, LieSupport & result);

/** \fn void Degree5_support_optimized(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								LieSupport & result)
	\brief Setting up a degree 5 cubature support, specialized for 1D equations.
	\param equation set of coefficient functions.
	\param *SetPoints is a pointer to a function setting up a Gaussian cubature formula.
	\param result the representation of the vector field valued random variable written into
*/
void Degree5_support_optimized(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								LieSupport & result);



/** \fn void Degree7_support_optimized_1D(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								LieSupport & result)
	\brief Setting up a degree 7 cubature support, specialized for 1D equations.
	\param equation set of coefficient functions.
	\param *SetPoints is a pointer to a function setting up a Gaussian cubature formula.
	\param result the representation of the vector field valued random variable written into
*/
void Degree7_support_optimized_1D(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								  LieSupport & result);


/** \fn void Degree7_support_optimized_2D(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								LieSupport & result)
	\brief Setting up a degree 7 cubature support, specialized for 2D equations.
	\param equation set of coefficient functions.
	\param *SetPoints is a pointer to a function setting up a Gaussian cubature formula.
	\param result the representation of the vector field valued random variable written into
*/
void Degree7_support_optimized_2D(Vec_ops & equation, void (*SetPoints)(Matrix &, Vector &),
								  LieSupport & result);



//*********************************************************
//*********************************************************
// AUXILIARY FUNCTIONS
//*********************************************************
//*********************************************************

namespace aux
{
/** \fn void void SupportSetting_Degree3(const DVec & eta_sup, Vector & eta, Vec_ops & equation, LieSupport & liesup,
							size_t & e, Scalar & weight)
	\brief Auxiliary function.
	\param eta_sup container of possible values for eta
	\param equation the set of coefficient functions (vector fields)
	\param liesup result written into

	Generating the support of the operator valued random variable, degree 3 case.
	Given possible outcomes of eta, generates all possible joint outcomes of etas and sigmas
	and the corresponding operators in the. Used in Degree3_support.
*/
void SupportSetting_Degree3(const DVec & eta_sup, Vector & eta, Vec_ops & equation, LieSupport & liesup,
							size_t & e, Scalar & weight);


/** \fn void SupportSetting(const DVec & eta_sup, const DVec & sigma_sup,
					 Vector & eta, Vector & sigma, const Vec_ops & equation, LieSupport & liesup,
					 size_t & e=size_t(0), size_t & s=size_t(0), Scalar & weight=Scalar(1))
	\brief Auxiliary function.
	\param eta_sup container of possible values for eta
	\patam sigma_sup container of possible values for sigma
	\param equation the set of coefficient functions (vector fields)
	\param liesup result written into

	Generating the support of the operator valued random variable.
	Given possible outcomes of eta and sigma, generates all possible joint outcomes of etas and sigmas
	and the corresponding operators in the. Used in Degree5_support.
*/
void SupportSetting(const DVec & eta_sup, const DVec & sigma_sup,
					 Vector & eta, Vector & sigma, const Vec_ops & equation, LieSupport & liesup,
					 size_t & e, size_t & s, Scalar & weight);


/** \fn dop::DifferentialOperator Degree3(const Vector & eta, Vec_ops & equation)
	\brief Auxiliary function.

	Given the vector of reslized eta, sets up the corresponding vector field.
	eta[]=[eta_1,...,eta_d]. Called from SupportSetting_Degree3.
*/
dop::DifferentialOperator Degree3_OneVF(const Vector & eta, Vec_ops & equation);


/** \fn dop::DifferentialOperator Degree5_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation)
	\brief Auxiliary function.

	Given vectors of reslized eta and sigma, sets up the corresponding vector field.
	eta[]=[eta_1,...,eta_d], sigma[]=[sigma_1,...,sigma_k], d(d-1)/2 many cross terms.
	Called from SupportSetting.
*/
dop::DifferentialOperator Degree5_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation);


/** \fn dop::DifferentialOperator Degree7_1D_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation)
	\brief Auxiliary function.

	Given vectors of reslized eta and sigma, sets up the corresponding vector field.
	eta[]=[eta_1,...,eta_d], sigma[]=[sigma_1,...,sigma_k].
	- e0 + x_i e1 + 1/12 [[e0,e1],e1]+ 1/sqrt(12) LAMBDA x_i [e0,e1] + 1/(45*8) [[[[e0,e1]e1]e1]e1]
*/
dop::DifferentialOperator Degree7_1D_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation);


/** \fn dop::DifferentialOperator Degree7_2D_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation)
	\brief Auxiliary function.

	Given vectors of reslized eta and sigma, sets up the corresponding vector field.
	Each result is a element in the support of the 2 dimensional degree 7 cubature formula. Derived by Christian Litterer.
*/
dop::DifferentialOperator Degree7_2D_OneVF(const Vector & eta, const Vector & sigma, const Vec_ops & equation);



/** \fn void GaussianCubature_Degree5_Dim2_size7(Matrix & points, Vector & weights)
	\brief Auxiliary function.

	Sets up degree 5 Gaussian cubature formula in Dim 4.
*/
void GaussianCubature_Degree5_Dim2_size7(Matrix & points, Vector & weights);


/** \fn void GaussianCubature_Degree5_Dim4_size25(Matrix & points, Vector & weights)
	\brief Auxiliary function.

	Sets up degree 5 Gaussian cubature formula in Dim 4.
*/
void GaussianCubature_Degree5_Dim4_size25(Matrix & points, Vector & weights);

/** \fn void GaussianCubature_Degree7_Dim1_size4(Matrix & points, Vector & weights)
	\brief Auxiliary function.

	Sets up degree 7 Gaussian cubature formula in Dim 1.
*/
void GaussianCubature_Degree7_Dim1_size4(Matrix & points, Vector & weights);


/** \fn void GaussianCubature_Degree7_Dim2_size12(Matrix & points, Vector & weights)
	\brief Auxiliary function.

	Sets up degree 7 Gaussian cubature formula in Dim 2.
*/
void GaussianCubature_Degree7_Dim2_size12(Matrix & points, Vector & weights);



}
}
#endif //_RANDOMLIESETTING_H__
