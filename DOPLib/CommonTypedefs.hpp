//Copyright (c) Lajos Gergely Gyurko, 2009-2013. All rights reserved.

#pragma once


#ifndef _COMMONTYPEDEFS_H__
#define _COMMONTYPEDEFS_H__

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>

namespace dop
{

#if __cplusplus > 199711L  
  #include <cstddef>
  using std::nullptr_t;  
#else 
    class nullptr_t
    {
        void operator&() const; // address of nullptr cannot be taken
    public:
        template< typename T >
        operator T *() const // non-member pointer
        {
           return 0;
        }

        template< typename C, typename T >
        operator T C::*() const // member pointer
        {
           return 0;
        }
    };
#endif

typedef double Scalar; //!< scalar type
typedef double Exponent; //!< type of exponents used in IMonomialComposite
typedef double Degree; //!< type of Degree used in Function
typedef size_t DiffIndex; //!< type of Diffindex used for partial differentiation, in particular in DifferentialOperators
typedef boost::numeric::ublas::vector<Scalar> Vector;//!< vector container type
typedef boost::numeric::ublas::matrix<Scalar> Matrix;//!< matrix container type

}

using dop::nullptr_t;

#endif //_COMMONTYPEDEFS_H__




