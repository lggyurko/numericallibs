//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once

#ifndef _DIFF_OP_PARTICULAR_SETUP_HPP___
#define _DIFF_OP_PARTICULAR_SETUP_HPP___


#include "DifferentialOperator_particular.hpp"

namespace dop
{

template<class VO>
class equation_setup
{
public:
	typedef VO Vec_ops;
	virtual ~equation_setup(){};
	virtual Vec_ops operator()()=0;
	unsigned int GetDim() {return iDim_;}

protected:
	unsigned int iDim_;
};


class geometricBM1D_setup : public dop::equation_setup<dop::Vec_ops>
{
public:
	typedef dop::equation_setup<dop::Vec_ops>::Vec_ops Vec_ops;

	geometricBM1D_setup(Scalar mu, Scalar sigma, Degree maxDegree=2.0, bool isStrat=false) :
	mu_(mu), sigma_(sigma), maxDegree_(maxDegree), isStrat_(isStrat) {iDim_=1;}


	Vec_ops operator()(){return GeometricBM1D(mu_, sigma_, maxDegree_, isStrat_);}

private:
	Scalar mu_;
	Scalar sigma_;
	Degree maxDegree_;
	bool isStrat_;
};


class geometricBMnAverage1D_setup : public equation_setup<dop::Vec_ops>
{
public:
	typedef dop::equation_setup<dop::Vec_ops>::Vec_ops Vec_ops;

	geometricBMnAverage1D_setup(Scalar mu, Scalar sigma, Degree maxDegree=2.0, bool isStrat=false) :
	mu_(mu), sigma_(sigma), maxDegree_(maxDegree), isStrat_(isStrat) {iDim_=1;}

	Vec_ops operator()(){	return GeometricBMnAverage1D(mu_, sigma_, maxDegree_, isStrat_);}

private:
	Scalar mu_;
	Scalar sigma_;
	Degree maxDegree_;
	bool isStrat_;
};

class geometricBM2D_setup : public equation_setup<dop::Vec_ops>
{
public:
	typedef dop::equation_setup<dop::Vec_ops>::Vec_ops Vec_ops;

	geometricBM2D_setup(Scalar mu1, Scalar sigma1, Scalar mu2, Scalar sigma2, Scalar rho,
								Degree maxDegree=2.0, bool isStrat=false) :
				mu1_(mu1), sigma1_(sigma1), mu2_(mu2), sigma2_(sigma2), rho_(rho),
				maxDegree_(maxDegree), isStrat_(isStrat) {iDim_=2;}

	Vec_ops operator()(){	return GeometricBM2D(mu1_, sigma1_, mu2_, sigma2_, rho_,
																		maxDegree_, isStrat_);}
private:
	Scalar mu1_;
	Scalar sigma1_;
	Scalar mu2_;
	Scalar sigma2_;
	Scalar rho_;
	Degree maxDegree_;
	bool isStrat_;
};


class stochasticCEVVol1D_setup : public equation_setup<dop::Vec_ops>
{
public:
	typedef dop::equation_setup<dop::Vec_ops>::Vec_ops Vec_ops;

	stochasticCEVVol1D_setup(Scalar mu, Scalar sigma, Scalar a, Scalar b, Scalar c, Scalar rho,
							Scalar e, Degree maxDegree=2.0, bool isStrat=false) :
				mu_(mu), sigma_(sigma), a_(a), b_(b), c_(c), rho_(rho), e_(e),
				maxDegree_(maxDegree), isStrat_(isStrat) {iDim_=2;}

	Vec_ops operator()(){return StochasticCEVVol1D(mu_, sigma_, a_, b_, c_, rho_,
																	e_,	maxDegree_, isStrat_);}
private:
	Scalar mu_;
	Scalar sigma_;
	Scalar a_;
	Scalar b_;
	Scalar c_;
	Scalar rho_;
	Scalar e_;
	Degree maxDegree_;
	bool isStrat_;
};


class stochasticCEVVolnAverage1D_setup : public equation_setup<dop::Vec_ops>
{
public:
	typedef dop::equation_setup<dop::Vec_ops>::Vec_ops Vec_ops;

	stochasticCEVVolnAverage1D_setup(Scalar mu, Scalar sigma, Scalar a, Scalar b, Scalar c, Scalar rho,
							Scalar e,Degree maxDegree=2.0, bool isStrat=false) :
				mu_(mu), sigma_(sigma), a_(a), b_(b), c_(c), rho_(rho), e_(e),
				maxDegree_(maxDegree), isStrat_(isStrat) {iDim_=2;}

	Vec_ops operator()(){	return StochasticCEVVolnAverage1D(mu_, sigma_, a_, b_, c_, rho_,
																	e_,	maxDegree_, isStrat_);}
private:
	Scalar mu_;
	Scalar sigma_;
	Scalar a_;
	Scalar b_;
	Scalar c_;
	Scalar rho_;
	Scalar e_;
	Degree maxDegree_;
	bool isStrat_;
};

}
#endif //_DIFF_OP_PARTICULAR_SETUP_HPP___
