//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once

#ifndef __FUNCTION_H__
#define __FUNCTION_H__

#include "IFunction.hpp"
#include "IFunction_particular.hpp"

namespace dop
{

class Function;

Function pow(const Function &,const Exponent);



/*! \class Function
	\brief Function object of vector to scalar functions.

	The class Function is a function object of vector to scalar
	functions. Object of this type can be added, multiplied
	and composed in many ways and also partial derivatives
	can be taken.
*/
class Function
{
	friend Function dop::pow(const Function &,const Exponent);
public:
	/**
	* Default constructor.
	* The default constructor is initialized with an IFunction_ptr pointing to a zero function.
	*/
	Function();

	/**
	* Copy constructor.
	* Clones ifun_ptr_
	*/
	Function(const Function &);

	/**
	* Copy assignment.
	* Clones ifun_ptr_
	*/
	Function & operator=(const Function & arg);

	/**
	* Consrtuctor from IFunction_ptr.
	* @param ptr is the IFunction_ptr.
	*/
	Function(IFunction_ptr ptr);

	/**
	* Scalar function constructor.
	* @param arg will be the return value when operator() is called.
	*/
	Function(Scalar arg);

	/**
	* Coordinate function constructor.
	* @param arg determines which coordinate to return when operator() is called.
	*/
	Function(size_t arg);

	/**
	* Power of a coordinate function constructor.
	* @param arg determines which coordinate to take power of when operator() is called.
	* @param exparg determines the exponent.
	*/
	Function(size_t arg, Exponent exparg);


	/**
	* The operator() is the key functionality.
	* @param arg is a Vector.
	* @return The evaluated result.
	*/
	Scalar operator()(const Vector & arg) const;

	/**
	* Checking if the functionality is equivalent to the zero fn.
	*/
	bool isZero() const;

	Function & operator+=(const Function &);
	Function & operator-=(const Function &);
	Function & operator*=(const Function &);
	Function & operator+=(const Scalar);
	Function & operator-=(const Scalar);
	Function & operator*=(const Scalar);

	/**
	* Partial differentiation.
	* @param arg is DiffIndex type
	* @return The partial derivative of the function.
	*/
	Function & Diff(DiffIndex);

	/**
	* Outputing the function.
	* Auxiliary member, mainly for testing purposes.
	*/
	std::ostream&  Print(std::ostream & os) const;


private:
	IFunction_ptr ifun_ptr_; //!< Smart pointer to the actual behavior.
};

inline std::ostream & operator<<(std::ostream& os, const Function & fun)
{
	return fun.Print(os);
}

Function operator+(const Function &, const Function &);
Function operator-(const Function &, const Function &);
Function operator*(const Function &, const Function &);
Function operator+(const Function &, const Scalar);
Function operator-(const Function &, const Scalar);
Function operator*(const Function &, const Scalar);
Function operator+(const Scalar, const Function &);
Function operator-(const Scalar, const Function &);
Function operator*(const Scalar, const Function &);
Function Diff(const Function &, DiffIndex);


}



#endif //__FUNCTION_H__


