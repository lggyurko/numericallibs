//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once

#ifndef _DIFF_OP_PARTICULAR_HPP___
#define _DIFF_OP_PARTICULAR_HPP___

#include <vector>
#include "DifferentialOperator.hpp"

namespace dop
{

typedef std::vector<DifferentialOperator> Vec_ops; //!< Vector of operators, i.e. the type of the equation

/** \fn Vec_ops GeometricBM1D(Scalar mu, Scalar sigma, Degree maxDegree)
  * \brief Function setting vector fields for the 1D geometric Brownian motion.
  * - dX=mu X dt + sigma X dB
  * @param mu is the drift coefficient
  * @param sigma is the volatility
  * @param maxDegree is the truncation level
  * @param isStrat if true the Stratonovich vector fields worked out, otherwise the Ito ones
  */
Vec_ops GeometricBM1D(Scalar mu, Scalar sigma, Degree maxDegree, bool isStrat=true);

/** \fn Vec_ops GeometricBMnAverage1D(Scalar mu, Scalar sigma, Degree maxDegree)
  * \brief Function setting vector fields for the 1D geometric Brownian motion with a time average term.
  * - dX=mu X dt + sigma X dB.
  * - dA=X dt.
  * @param mu is the drift coefficient
  * @parma sigma is the volatility
  * @param maxDegree is the truncation level
  * @param isStrat if true the Stratonovich vector fields worked out, otherwise the Ito ones
  */
Vec_ops GeometricBMnAverage1D(Scalar mu, Scalar sigma, Degree maxDegree, bool isStrat=true);

/** \fn Vec_ops GeometricBM2D(Scalar mu1, Scalar sigma1, Scalar mu2, Scalar sigma2, Scalar rho)
  * \brief Function setting vector fields for the 2D geometric Brownian motion with a time average term.
  * - dX1=mu1 X1 dt + sigma1 X1 dB1.
  * - dX2=mu2 X2 dt + sigma2 rho X2 dB1 + sigma2 sqrt(1-rho^2) X2 dB2
  * @param mu1 is the drift coefficient of X1
  * @param mu2 is the drift coefficient of X2
  * @parma sigma1 is the volatility of X1
  * @parma sigma2 is the volatility of X2
  * @parma rho the correlation of the driving Brownian motions
  * @param maxDegree is the truncation level
  * @param isStrat if true the Stratonovich vector fields worked out, otherwise the Ito ones
  */
Vec_ops GeometricBM2D(Scalar mu1, Scalar sigma1, Scalar mu2,
					  Scalar sigma2, Scalar rho, Degree maxDegree, bool isStrat=true);

/** \fn Vec_ops StochasticCEVVol1D(Scalar mu, Scalar sigma, Scalar a, Scalar b, Scalar c, Scalar rho, Exponenet e)
  * \brief Function setting vector fields for the 2D geometric Brownian motion with a time average term.
  * - dX=mu X dt + sigma sqrt(V) X dB1.
  * - dV=a(b-V)dt + c V^e(rho dB1+sqrt(1-rho^2) dB2)
  * @param mu is the drift coefficient
  * @parma sigma is the volatility
  * @param a is the mean reverting speed of the vol
  * @param b is the mean reverting level of the vol
  * @param c is the volatility parameter of the vol
  * @param rho is the correlation of the driving Brownian motions
  * @param e is the exponent used in the vol of vol
  * @param maxDegree is the truncation level
  * @param isStrat if true the Stratonovich vector fields worked out, otherwise the Ito ones
  */
Vec_ops StochasticCEVVol1D(Scalar mu, Scalar sigma, Scalar a, Scalar b,
						   Scalar c, Scalar rho, Exponent e, Degree maxDegree, bool isStrat=true);


/** \fn Vec_ops StochasticCEVVolnAverage1D(Scalar mu, Scalar sigma, Scalar a, Scalar b, Scalar c, Scalar rho, Exponenet e)
  * \brief Function setting vector fields for the 2D geometric Brownian motion with a time average term.
  * - dX=mu X dt + sigma sqrt(V) X dB1.
  * - dA=Xdt.
  * - dV=a(b-V)dt + c V^e(rho dB1+sqrt(1-rho^2) dB2).
  * @param mu is the drift coefficient
  * @parma sigma is the volatility
  * @param a is the mean reverting speed of the vol
  * @param b is the mean reverting level of the vol
  * @param c is the volatility parameter of the vol
  * @param rho is the correlation of the driving Brownian motions
  * @param e is the exponent used in the vol of vol
  * @param maxDegree is the truncation level
  * @param isStrat if true the Stratonovich vector fields worked out, otherwise the Ito ones
  */
Vec_ops StochasticCEVVolnAverage1D(Scalar mu, Scalar sigma, Scalar a, Scalar b,
						   Scalar c, Scalar rho, Exponent e, Degree maxDegree, bool isStrat=true);



}

#endif //_DIFF_OP_PARTICULAR_HPP___

