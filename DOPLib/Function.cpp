//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.

#include "Function.hpp"

using namespace dop;
using namespace dop::aux;

dop::Function::Function() : ifun_ptr_(new ISumComposite) {}

dop::Function::Function(const Function & arg) : ifun_ptr_(arg.ifun_ptr_->Clone()) {}

Function & dop::Function::operator=(const Function & arg)
{
	ifun_ptr_=arg.ifun_ptr_->Clone();
	return *this;
}

dop::Function::Function(IFunction_ptr ptr) : ifun_ptr_(ptr->Clone()) {}

dop::Function::Function(Scalar arg) : ifun_ptr_(new ISumComposite(arg)) {}


dop::Function::Function(size_t arg) : ifun_ptr_(ICoordinate::Instance(arg))
{
    IFunction_ptr temp_ptr(new ISumComposite(Scalar(1)));
	SingleDiffMap::Insert(ifun_ptr_,DiffIndex(arg),temp_ptr);
}


dop::Function::Function(size_t arg, Exponent exparg) :
				ifun_ptr_(dop::pow(ICoordinate::Instance(arg),exparg))
{
    IFunction_ptr temp_ptr1(ICoordinate::Instance(arg));
    IFunction_ptr temp_ptr2(new ISumComposite(Scalar(1)));
    SingleDiffMap::Insert(temp_ptr1,DiffIndex(arg),temp_ptr2);
}


Scalar dop::Function::operator ()(const Vector & arg) const
{
	return ifun_ptr_->operator ()(arg);
}

bool dop::Function::isZero() const
{
	return ifun_ptr_->isZero();
}

Function & dop::Function::operator +=(const Function & arg)
{
	IFunction_ptr IFptr(ifun_ptr_+arg.ifun_ptr_);
	ifun_ptr_=IFptr;
	return *this;
}

Function & dop::Function::operator -=(const Function & arg)
{
	IFunction_ptr IFptr(ifun_ptr_+(arg.ifun_ptr_*(Scalar(-1))));
	ifun_ptr_=IFptr;
	return *this;
}

Function & dop::Function::operator *=(const Function & arg)
{
	IFunction_ptr IFptr(ifun_ptr_*arg.ifun_ptr_);
	ifun_ptr_=IFptr;
	return *this;
}

Function & dop::Function::operator +=(const Scalar arg)
{
	IFunction_ptr IFptr(ifun_ptr_+arg);
	ifun_ptr_=IFptr;
	return *this;
}

Function & dop::Function::operator -=(const Scalar arg)
{
	IFunction_ptr IFptr(ifun_ptr_+(Scalar(-1)*arg));
	ifun_ptr_=IFptr;
	return *this;
}

Function & dop::Function::operator *=(const Scalar arg)
{
	IFunction_ptr IFptr(ifun_ptr_*arg);
	ifun_ptr_=IFptr;
	return *this;
}


Function dop::operator +(const Function & arg1,const Function & arg2)
{
	Function result(arg1);
	result+=arg2;
	return result;
}

Function dop::operator -(const Function & arg1,const Function & arg2)
{
	Function result(arg1);
	result-=arg2;
	return result;
}

Function dop::operator *(const Function & arg1,const Function & arg2)
{
	Function result(arg1);
	result*=arg2;
	return result;
}


Function dop::operator +(const Function & arg1,const Scalar arg2)
{
	Function result(arg1);
	result+=arg2;
	return result;
}

Function dop::operator -(const Function & arg1,const Scalar arg2)
{
	Function result(arg1);
	result-=arg2;
	return result;
}

Function dop::operator *(const Function & arg1,const Scalar arg2)
{
	Function result(arg1);
	result*=arg2;
	return result;
}


Function dop::operator +(const Scalar arg2, const Function & arg1)
{
	return arg1+arg2;
}

Function dop::operator -(const Scalar arg2, const Function & arg1)
{
	return arg1*(Scalar(-1))+arg2;
}

Function dop::operator *(const Scalar arg2, const Function & arg1)
{
	return arg1*arg2;
}

Function dop::pow(const dop::Function & arg1,const Exponent arg2)
{
	return Function(pow(arg1.ifun_ptr_,arg2));

}

Function & dop::Function::Diff(DiffIndex arg)
{
	ifun_ptr_=ifun_ptr_->Diff(arg);
	return *this;
}

Function dop::Diff(const Function & arg1, DiffIndex arg2)
{
	Function result(arg1);
	return result.Diff(arg2);
}


std::ostream&  dop::Function::Print(std::ostream & os) const
{
		os<< std::endl;
		os<< "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
		os<< "<<<FUNCTION_start " << std::endl;
		ifun_ptr_->Print(os);
		os<< " FUNCTION_finish>>> " << std::endl;
		os<< ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
		os<< std::endl;
		return os;
}
