//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.
#pragma once


#ifndef __ODESOLVER_H___
#define __ODESOLVER_H___

#include <vector>
#include "DifferentialOperator.hpp"

namespace dop
{

/** \class ODESolverRK
  * \brief General implementation of explicit Runge-Kutta methods
  *
  * The implementation follows Numerical Methods for Ordinary Differential Equations by Butcher.
  * The data members implement the table representation (vectors b and matrix A).
  * Note that since the problem is specialized for autonomous ODEs, the vector c
  * is not used, so c is not kept as data element.
  * An extended verion is required for the non-autonomous ODEs.
*/
class ODESolverRK
{
public:
	/** Constructor
	  * @param b_arg vector b
	  * @param A_arg matrix A
	  * @param steps_arg number of substeps
	  * @param Truncate_arg if true, the first coordinate is truncated at zero
	  * @param coord if Truncate_ true, the coordinate corresping to coordinate_ is truncated at zero
	  */
	ODESolverRK(Vector & b_arg, Matrix & A_arg,
		size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
		: b_(b_arg), A_(A_arg), steps_(steps_arg), Truncate_(Truncate_arg), coordinate_(coord)
	{}

	/** function implementing the numerical step
	  * @param VF the vector field
	  * @param vecIn the initial condition
	  * @param vecOut the result is written into
	  * @param scale rescaling parameter (dt)
	  */
	void operator()(const DifferentialOperator & VF, const Vector & vecIn,
								Vector & vecOut, Scalar scale);

private:
	Vector b_;
	Matrix A_;
	size_t steps_; //!< number of steps (on an equi-distant finer scale)
	bool Truncate_; //!< if Truncate_ true, the coordinate corresping to coordinate_ is truncated at zero
	size_t coordinate_; //!< if Truncate_ true, the coordinate corresping to coordinate_ is truncated at zero


};

/*! \fn ODESolverRK ConstructRK2(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
   	\brief Consrtucts the classical order 2 Runge-Kutta scheme.

    Based on page 94 of Butcher
*/
ODESolverRK ConstructRK2(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0);

/*! \fn ODESolverRK ConstructRK3(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
   	\brief Consrtucts the classical order 3 Runge-Kutta scheme.

    Based on page 171 of Butcher, Case II
*/
ODESolverRK ConstructRK3(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0);

/*! \fn ODESolverRK ConstructRK4(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
   	\brief Consrtucts the classical order 4 Runge-Kutta scheme.

    Based on page 180 of Butcher
*/
ODESolverRK ConstructRK4(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0);

/*! \fn ODESolverRK ConstructRK5(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
   	\brief Consrtucts an order 5 Runge-Kutta scheme.

   Based on page 192 of Butcher
*/
ODESolverRK ConstructRK5(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0);

/*! \fn ODESolverRK ConstructRK6(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
   	\brief Consrtucts an order 6 Runge-Kutta scheme.

    Based on page 194 of Butcher
*/
ODESolverRK ConstructRK6(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0);

/*! \fn ODESolverRK ConstructRK7(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0)
   	\brief Consrtucts an order 7 Runge-Kutta scheme.

    Based on page 196 of Butcher
*/
ODESolverRK ConstructRK7(size_t steps_arg=1, bool Truncate_arg=false, size_t coord=0);




}
#endif //__ODESOLVER_H___
