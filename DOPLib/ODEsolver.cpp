//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved. 

#ifdef MSC_VER
#pragma warning(disable:4996)
#endif 

#include "ODEsolver.hpp"


using namespace dop;

void dop::ODESolverRK::operator()(const DifferentialOperator & VF, const Vector & vecIn, 
								Vector & vecOut, Scalar scale)
{
	Scalar tiny=1.0e-18;
	Scalar h=(Scalar(1.0)/Scalar(steps_));

	Vector vecOld(vecIn.size());
	vecOut=vecIn;
	for(size_t counter=0; counter<steps_; ++counter)
	{
		vecOld=vecOut;
		std::vector<Vector> k;
		//k_1=f(y);
		Vector tempVec(vecIn.size());
		if(Truncate_)
		{
			vecOld(coordinate_)=std::max<Scalar>(vecOld(0),tiny);
		}
		VF(vecOld,tempVec,scale);
		k.push_back(tempVec);
		//y_new=y+h*sum_i b_ik_i
		vecOut+=tempVec*(b_(0)*h);
			
		//k_i=f(y+sum_{j<i}A_{r,j}hk_j)
		for(size_t RowIndex=0; RowIndex<A_.size1(); RowIndex++)
		{
			Vector inner_tempVec(vecOld);
			for(size_t index=0; index< RowIndex+1; index++)
			{
				inner_tempVec+=k[index]*(A_(RowIndex,index)*h);
				
			}
			if(Truncate_)
			{
				inner_tempVec(coordinate_)=std::max<Scalar>(inner_tempVec(0),tiny);
			}
			VF(inner_tempVec,tempVec,scale);
			k.push_back(tempVec);
			//y_new=y+h*sum_i b_ik_i
			vecOut+=tempVec*(b_(RowIndex+1)*h);
		}
	}
}



ODESolverRK dop::ConstructRK2(size_t steps_arg, bool Truncate_arg, size_t coord)
{
	Vector b_arg(2);
	b_arg(0)=Scalar(0.5);
	b_arg(1)=Scalar(0.5);
	
	/*Vector c_arg;
	c_arg(0)=Scalar(0.0);
	c_arg(1)=Scalar(1.0);*/
	
	Matrix A_arg(1,1);
	A_arg(0,0)=Scalar(1.0);
		
	return ODESolverRK(b_arg, A_arg, steps_arg, Truncate_arg, coord); 
}

ODESolverRK dop::ConstructRK3(size_t steps_arg, bool Truncate_arg, size_t coord)
{
	//Butcher: page 171, classical Case II
	Scalar b3=0.5;
	Vector b_arg(3);
	b_arg(0)=Scalar(0.25);
	b_arg(1)=Scalar(0.75-1.0/b3);
	b_arg(2)=Scalar(b3);
	
	/*Vector c_arg;
	c_arg(0)=Scalar(0.0);
	c_arg(1)=Scalar(2.0/3.0);
	c_arg(2)=Scalar(0.0);*/
	
	Matrix A_arg(2,2);
	A_arg(0,0)=Scalar(2.0/3.0);
	A_arg(1,0)=Scalar(2.0/3.0-1.0/(4.0*b3));
	A_arg(1,1)=Scalar(1.0/(4.0*b3));
	
	return ODESolverRK(b_arg, A_arg, steps_arg, Truncate_arg, coord); 
}

ODESolverRK dop::ConstructRK4(size_t steps_arg, bool Truncate_arg, size_t coord)
{
	//Butcher: page 180, classical RK4
	Vector b_arg(4);
	b_arg(0)=Scalar(1.0/6.0);
	b_arg(1)=Scalar(1.0/3.0);
	b_arg(2)=Scalar(1.0/3.0);
	b_arg(3)=Scalar(1.0/6.0);

	/*Vector c_arg(4);
	c_arg(0)=Scalar(0.0);
	c_arg(1)=Scalar(0.5);
	c_arg(2)=Scalar(0.5);
	c_arg(3)=Scalar(1.0);*/

	Matrix A_arg(3,3);
	A_arg(0,0)=Scalar(0.5);
	A_arg(1,0)=Scalar(0.0);
	A_arg(1,1)=Scalar(0.5);
	A_arg(2,0)=Scalar(0.0);
	A_arg(2,1)=Scalar(0.0);
	A_arg(2,2)=Scalar(1.0);

	return ODESolverRK(b_arg, A_arg, steps_arg, Truncate_arg, coord); 
}

ODESolverRK dop::ConstructRK5(size_t steps_arg, bool Truncate_arg, size_t coord)
{
	//Butcher: page 192, RK5
	Vector b_arg(6);
	b_arg(0)=Scalar(23.0/192.0);
	b_arg(1)=Scalar(0.0);
	b_arg(2)=Scalar(125.0/192.0);
	b_arg(3)=Scalar(0.0);
	b_arg(4)=Scalar(-27.0/64.0);
	b_arg(5)=Scalar(125.0/192.0);

	/*Vector c_arg(6);
	c_arg(0)=Scalar(0.0);
	c_arg(1)=Scalar(1.0/3.0);
	c_arg(2)=Scalar(0.4);
	c_arg(3)=Scalar(1.0);
	c_arg(4)=Scalar(2.0/3.0);
	c_arg(5)=Scalar(0.8);*/

	Matrix A_arg(5,5);
	A_arg(0,0)=Scalar(1.0/3.0);
	A_arg(1,0)=Scalar(4.0/25.0);
	A_arg(1,1)=Scalar(6.0/25.0);
	A_arg(2,0)=Scalar(0.25);
	A_arg(2,1)=Scalar(-3.0);
	A_arg(2,2)=Scalar(15.0/4.0);
	A_arg(3,0)=Scalar(2.0/27.0);
	A_arg(3,1)=Scalar(10.0/9.0);
	A_arg(3,2)=Scalar(-50.0/81.0);
	A_arg(3,3)=Scalar(8.0/81.0);
	A_arg(4,0)=Scalar(2.0/25.0);
	A_arg(4,1)=Scalar(12.0/25.0);
	A_arg(4,2)=Scalar(2.0/15.0);
	A_arg(4,3)=Scalar(8.0/75.0);
	A_arg(4,4)=Scalar(0.0);
	

	return ODESolverRK(b_arg, A_arg, steps_arg, Truncate_arg,coord); 
}


ODESolverRK dop::ConstructRK6(size_t steps_arg, bool Truncate_arg, size_t coord)
{
	//Butcher: page 194, RK6
	Vector b_arg(7);
	b_arg(0)=Scalar(13.0/200.0);
	b_arg(1)=Scalar(0.0);
	b_arg(2)=Scalar(11.0/40.0);
	b_arg(3)=Scalar(11.0/40.0);
	b_arg(4)=Scalar(4.0/25.0);
	b_arg(5)=Scalar(4.0/25.0);
	b_arg(6)=Scalar(13.0/200.0);

	/*Vector c_arg(7);
	c_arg(0)=Scalar(0.0);
	c_arg(1)=Scalar(1.0/3.0);
	c_arg(2)=Scalar(2.0/3.0);
	c_arg(3)=Scalar(1.0/3.0);
	c_arg(4)=Scalar(5.0/6.0);
	c_arg(5)=Scalar(1.0/6.0);
	c_arg(6)=Scalar(1.0);*/

	Matrix A_arg(6,6);
	A_arg(0,0)=Scalar(1.0/3.0);
	A_arg(1,0)=Scalar(0.0);
	A_arg(1,1)=Scalar(2.0/3.0);
	A_arg(2,0)=Scalar(1.0/12.0);
	A_arg(2,1)=Scalar(1.0/3.0);
	A_arg(2,2)=Scalar(-1.0/12.0);
	A_arg(3,0)=Scalar(25.0/48.0);
	A_arg(3,1)=Scalar(-55.0/24.0);
	A_arg(3,2)=Scalar(35.0/48.0);
	A_arg(3,3)=Scalar(15.0/8.0);
	A_arg(4,0)=Scalar(3.0/20.0);
	A_arg(4,1)=Scalar(-11.0/24.0);
	A_arg(4,2)=Scalar(-1.0/8.0);
	A_arg(4,3)=Scalar(0.5);
	A_arg(4,4)=Scalar(0.1);
	A_arg(5,0)=Scalar(-261.0/260.0);
	A_arg(5,1)=Scalar(33.0/13.0);
	A_arg(5,2)=Scalar(43.0/156.0);
	A_arg(5,3)=Scalar(-118.0/39.0);
	A_arg(5,4)=Scalar(32.0/195.0);
	A_arg(5,5)=Scalar(80.0/39.0);
	

	return ODESolverRK(b_arg, A_arg, steps_arg, Truncate_arg, coord); 
}


ODESolverRK dop::ConstructRK7(size_t steps_arg, bool Truncate_arg, size_t coord)
{
	//Butcher: page 196, RK7
	Vector b_arg(9);
	b_arg(0)=Scalar(0.0);
	b_arg(1)=Scalar(0.0);
	b_arg(2)=Scalar(0.0);
	b_arg(3)=Scalar(32.0/105.0);
	b_arg(4)=Scalar(1771561.0/6289920.0);
	b_arg(5)=Scalar(243.0/2560.0);
	b_arg(6)=Scalar(16807.0/74880.0);
	b_arg(7)=Scalar(77.0/1440.0);
	b_arg(8)=Scalar(11.0/270.0);

	/*Vector c_arg(9);
	c_arg(0)=Scalar(0.0);
	c_arg(1)=Scalar(1.0/6.0);
	c_arg(2)=Scalar(1.0/3.0);
	c_arg(3)=Scalar(1.0/2.0);
	c_arg(4)=Scalar(2.0/11.0);
	c_arg(5)=Scalar(2.0/3.0);
	c_arg(6)=Scalar(6.0/7.0);
	c_arg(7)=Scalar(0.0);
	c_arg(8)=Scalar(1.0);*/

	Matrix A_arg(8,8);
	
	A_arg(0,0)=Scalar(1.0/6.0);

	A_arg(1,0)=Scalar(0.0);
	A_arg(1,1)=Scalar(1.0/3.0);
	
	A_arg(2,0)=Scalar(1.0/8.0);
	A_arg(2,1)=Scalar(0.0);
	A_arg(2,2)=Scalar(3.0/8.0);
	
	A_arg(3,0)=Scalar(148.0/1331.0);
	A_arg(3,1)=Scalar(0.0);
	A_arg(3,2)=Scalar(150.0/1331.0);
	A_arg(3,3)=Scalar(-56.0/1331.0);
	
	A_arg(4,0)=Scalar(-404.0/243.0);
	A_arg(4,1)=Scalar(0.0);
	A_arg(4,2)=Scalar(-170.0/27.0);
	A_arg(4,3)=Scalar(4024.0/1701.0);
	A_arg(4,4)=Scalar(10648.0/1701.0);
	
	A_arg(5,0)=Scalar(2466.0/2401.0);
	A_arg(5,1)=Scalar(0.0);
	A_arg(5,2)=Scalar(1242.0/343.0);
	A_arg(5,3)=Scalar(-19176.0/16807.0);
	A_arg(5,4)=Scalar(-51909.0/16807.0);
	A_arg(5,5)=Scalar(1053.0/2401.0);
	
	A_arg(6,0)=Scalar(5.0/154.0);
	A_arg(6,1)=Scalar(0.0);
	A_arg(6,2)=Scalar(0.0);
	A_arg(6,3)=Scalar(96.0/539.0);
	A_arg(6,4)=Scalar(-1815.0/20384.0);
	A_arg(6,5)=Scalar(-405.0/2464.0);
	A_arg(6,6)=Scalar(49.0/1144.0);

	A_arg(7,0)=Scalar(-113.0/32.0);
	A_arg(7,1)=Scalar(0.0);
	A_arg(7,2)=Scalar(-195.0/22.0);
	A_arg(7,3)=Scalar(32.0/7.0);
	A_arg(7,4)=Scalar(29403.0/3584.0);
	A_arg(7,5)=Scalar(-729.0/512.0);
	A_arg(7,6)=Scalar(1029.0/1408.0);
	A_arg(7,7)=Scalar(21.0/16.0);

	return ODESolverRK(b_arg, A_arg, steps_arg, Truncate_arg, coord); 
}
