//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved. 

#include "IFunction.hpp"

#include <cmath>
//
using namespace dop;
using namespace dop::aux;

void InsertIntoMonomial(dop::IMonomialComposite::MonomialMap & monomial_, 
						const IFunction_ptr & arg, Exponent exarg=Exponent(1));


//**************************************************************
//**************************************************************
// IFUNCTION
//**************************************************************
//**************************************************************



IFunction_ptr dop::operator+(const IFunction_ptr & arg1, const IFunction_ptr & arg2)
{
	if(arg1->isZero())
		return arg2->Clone();

	if(arg2->isZero())
		return arg1->Clone();

	ISumComposite * result=new ISumComposite;
	result->operator+=(arg1);
	result->operator+=(arg2);
	std::pair<bool,IFunction_ptr> testPair=result->isSingleTerm();
	if(testPair.first)
	{
		delete result;
		return testPair.second;
	}
	else
		return IFunction_ptr(result);
}
IFunction_ptr dop::operator+(const IFunction_ptr & arg1, const Scalar arg2)
{
	if(arg1->isZero())
		return IFunction_ptr(new ISumComposite(arg2));
	if(arg2==Scalar(0))
		return arg1->Clone();

	ISumComposite * result=new ISumComposite;
	result->operator+=(arg1); //using ISumComposit::operator+=
	result->operator+=(arg2); //using ISumComposit::operator+=
	return IFunction_ptr(result);
}
IFunction_ptr dop::operator+(const Scalar arg1, const IFunction_ptr & arg2)
{
	if(arg2->isZero())
		return IFunction_ptr(new ISumComposite(arg1));
	if(arg1==Scalar(0))
		return arg2->Clone();

	ISumComposite * result=new ISumComposite;
	result->operator+=(arg1); //using ISumComposite::operator+=
	result->operator+=(arg2); //using ISumComposite::operator+=
	return IFunction_ptr(result);
}
IFunction_ptr dop::operator*(const IFunction_ptr & arg1, const IFunction_ptr & arg2)
{
	if(arg1->isZero() || arg2->isZero()) // check for zero
		return IFunction_ptr(new ISumComposite);
	
	std::pair<bool, const ISumComposite *> scalarTest1=arg1->isScalar(); 
	if(scalarTest1.first && scalarTest1.second->GetAddConst()==Scalar(1)) // check for 1
	{
		if(arg1->isMc().first)
			delete scalarTest1.second;
		return arg2->Clone();
	}

	std::pair<bool, const ISumComposite *> scalarTest2=arg2->isScalar();
	if(scalarTest2.first && scalarTest2.second->GetAddConst()==Scalar(1)) // check for 1
	{
		if(arg2->isMc().first)
			delete scalarTest2.second;
		return arg1->Clone();
	}

	std::pair<bool, const ISumComposite *> sumTest1=arg1->isSc();
	if(sumTest1.first) // otherwise
	{
		ISumComposite * result=new ISumComposite(*sumTest1.second);
		result->operator*=(arg2); //using ISumComposite::operator*=
		std::pair<bool,IFunction_ptr> testPair=result->isSingleTerm();
		if(testPair.first)
		{
			delete result;
			return testPair.second;
		}
		else
			return IFunction_ptr(result);
	}

	std::pair<bool, const ISumComposite *> sumTest2=arg2->isSc();
	if(sumTest2.first)
	{
		ISumComposite * result=new ISumComposite(*sumTest2.second);
		result->operator*=(arg1); //using ISumComposite::operator*=
		std::pair<bool,IFunction_ptr> testPair=result->isSingleTerm();
		if(testPair.first)
		{
			delete result;
			return testPair.second;
		}
		else
			return IFunction_ptr(result);
	}
	else //neither is SC
	{
		std::pair<bool, const IMonomialComposite *> monTest1=arg1->isMc();
		if(monTest1.first)
		{
			IMonomialComposite * result=new IMonomialComposite(*monTest1.second);
			result->operator*=(arg2); //using IMonomialComposite::operator*=
			return IFunction_ptr(result);
		}
		std::pair<bool, const IMonomialComposite *> monTest2=arg2->isMc();
		if(monTest2.first)
		{
			IMonomialComposite * result=new IMonomialComposite(*monTest2.second);
			result->operator*=(arg1);//using IMonomialComposite::operator*=
			return IFunction_ptr(result);
		}
		else // neither is MC
		{
			IMonomialComposite * result=new IMonomialComposite;
			result->operator*=(arg1);//using IMonomialComposite::operator*=
			result->operator*=(arg2);//using IMonomialComposite::operator*=
			return IFunction_ptr(result);
		}
	}
	
}

IFunction_ptr dop::operator*(const IFunction_ptr & IFptr, const Scalar arg)
{
	if(arg==Scalar(0))
	{
		return IFunction_ptr(new ISumComposite);
	}
	else if(arg==Scalar(1))
	{
		return IFunction_ptr(IFptr->Clone());
	}
	else
	{
		std::pair<bool, const ISumComposite *> sumTest=IFptr->isSc();
		if(sumTest.first)
		{	
			ISumComposite * result=new ISumComposite(*(sumTest.second));
			result->operator*=(arg);
			return IFunction_ptr(result);
		}
		else
		{
			std::pair<bool, const IMonomialComposite *> monTest=IFptr->isMc();
			if(monTest.first)
			{	
				IMonomialComposite * result=new IMonomialComposite(*(monTest.second));
				result->operator*=(arg);
				return IFunction_ptr(result);
			}
			else
			{
				IMonomialComposite * result=new IMonomialComposite(IFptr);
				result->operator*=(arg);
				return IFunction_ptr(result);
			}
		}
	}
}


IFunction_ptr dop::operator*(const Scalar arg1, const IFunction_ptr & arg2)
{
	return arg2*arg1;
}

IFunction_ptr dop::pow(const IFunction_ptr & arg1, const Exponent arg2)
{

	std::pair<bool,const ISumComposite *> scalarTest=arg1->isScalar();
	if(scalarTest.first)
	{
		ISumComposite * result=new ISumComposite;
		result->addConst_=std::pow(scalarTest.second->addConst_,arg2);
		if(arg1->isMc().first)
			delete scalarTest.second;
		return IFunction_ptr(result);
	}

	std::pair<bool,const IMonomialComposite *> monTest=arg1->isMc();
	if(monTest.first)
	{
		IMonomialComposite * result=new IMonomialComposite(*monTest.second);
		result->pow(arg2);
		return IFunction_ptr(result);
	}
	else
	{
		IMonomialComposite * result=new IMonomialComposite(arg1,arg2);
		return IFunction_ptr(result);
	}
}


//**************************************************************
//**************************************************************
// ISUMCOMPOSITE
//**************************************************************
//**************************************************************

dop::ISumComposite::ISumComposite(Scalar arg) : addConst_(arg)
{}

dop::ISumComposite::ISumComposite(const ISumComposite & arg) : addConst_(arg.addConst_)
{

	SumSet::const_iterator e_iter=arg.sum_.end();
	for(SumSet::const_iterator iter=arg.sum_.begin(); iter!=e_iter; ++iter)
		sum_.insert((*iter)->Clone());
}

IFunction_ptr dop::ISumComposite::Clone() 
{
	ISumComposite * result=new ISumComposite(*this);
	return IFunction_ptr(result);
}

ISumComposite & dop::ISumComposite::operator=(const ISumComposite & arg)
{
	addConst_=arg.addConst_;
	sum_.clear();
	SumSet::const_iterator e_iter=arg.sum_.end();
	for(SumSet::const_iterator iter=arg.sum_.begin(); iter!=e_iter; ++iter)
		sum_.insert((*iter)->Clone());	
	return *this;
}

std::pair<bool, ISumComposite *> dop::ISumComposite::isSc() 
{
	return std::pair<bool, ISumComposite *>(true,this);
}

std::pair<bool, ISumComposite *> dop::ISumComposite::isScalar()
{
	if(sum_.empty())
		return std::pair<bool, ISumComposite *>(true, this);
	else
		return std::pair<bool, ISumComposite *>(false,nullptr_t());
}

bool dop::ISumComposite::isZero() const
{
	return sum_.empty() && addConst_==Scalar(0);
}

std::ostream&  dop::ISumComposite::Print(std::ostream & os) const
{
	SumSet::const_iterator e_iter=sum_.end();
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	os<< " <<<SumComp_start " << std::endl;	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	for(SumSet::const_iterator iter=sum_.begin(); iter!=e_iter;++iter)
	{
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		(*iter)->Print(os);
		os<< " + " << std::endl;
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	os<< addConst_	<< std::endl; 
	os<< " SumComp_finish>>> "	<< std::endl; 
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	return os;
}

ISumComposite & dop::ISumComposite::operator +=(const Scalar arg)
{
	addConst_+=arg;
	return *this;
}

ISumComposite & dop::ISumComposite::operator +=(const dop::IFunction_ptr & arg)
{
	if(!(arg->isZero())) // only act if nonzero
	{
		std::pair<bool, const ISumComposite *> scalarTest=arg->isScalar();
		if(scalarTest.first) //IF SCALAR
		{
			addConst_+=scalarTest.second->addConst_;
			if(arg->isMc().first)
				delete scalarTest.second;
		}
		else //IF NON SCALAR
		{
			std::pair<bool, const ISumComposite *> sumTest=arg->isSc();
			if(sumTest.first) // IF SUMCOMPOSITE then "merge"
			{
				const ISumComposite & argSC=*(sumTest.second);
				//sort out scalar part:
				addConst_+=argSC.addConst_;
				//then merge the sum_ parts		
				SumSet::const_iterator e_iter=argSC.sum_.end();
				for(SumSet::const_iterator iter=argSC.sum_.begin(); iter!=e_iter; ++iter)
				{
					if(!(*iter)->isZero())//only act if nonzero
					{
						SumSet::iterator s_iter=sum_.find(*iter);
						if(s_iter==sum_.end()) // if not in yet, then insert
							sum_.insert((*iter)->Clone());
						else //if already a term, if monomial -> increase scalar part, otherwise make it monomial with weight 2
						{
							std::pair<bool, const IMonomialComposite *> s_monTest=(*s_iter)->isMc();
							std::pair<bool, const IMonomialComposite *> monTest=(*iter)->isMc();
							if(s_monTest.first && monTest.first) //both MC
							{
								Scalar sc(s_monTest.second->multConst_+monTest.second->multConst_);
								if(sc==Scalar(0))
									sum_.erase(s_iter);
								else
								{
									IMonomialComposite * newMc=new IMonomialComposite((*s_iter)->Clone());
									newMc->multConst_=sc;
									sum_.erase(s_iter);
									std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
									assert(insertTest.second);
								}
							}
							else if(s_monTest.first) //only first MC
							{
								Scalar sc(s_monTest.second->multConst_+Scalar(1));
								if(sc==Scalar(0))
									sum_.erase(s_iter);
								else
								{
									IMonomialComposite * newMc=new IMonomialComposite((*s_iter)->Clone());
									newMc->multConst_=sc;
									sum_.erase(s_iter);
									std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
									assert(insertTest.second);
								}
							}
							else if(monTest.first) //only second MC
							{
								Scalar sc(monTest.second->multConst_+Scalar(1));
								if(sc==Scalar(0))
									sum_.erase(s_iter);
								else
								{
									IMonomialComposite * newMc=new IMonomialComposite((*iter)->Clone());
									newMc->multConst_=sc;
									sum_.erase(s_iter);
									std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
									assert(insertTest.second);
								}
							}
							else //non of them is MC
							{
								IMonomialComposite * newMc=new IMonomialComposite((*s_iter)->Clone());
								newMc->multConst_=Scalar(2);
								sum_.erase(s_iter);
								std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
								assert(insertTest.second);
							}
						}
					}
				}
			}
			else // not SUMCOMPOSITE, "insert"
			{
				SumSet::iterator s_iter=sum_.find(arg);
				if(s_iter==sum_.end()) //if not in, simpe insert
				{
					std::pair<SumSet::iterator,bool> insertTest=sum_.insert(arg->Clone());
					assert(insertTest.second);
				}
				else //already in, then adjust weight
				{
					std::pair<bool, const IMonomialComposite *> s_monTest=(*s_iter)->isMc();
					std::pair<bool, const IMonomialComposite *> monTest=arg->isMc();
					if(s_monTest.first && monTest.first) //both MC
					{
						Scalar sc(s_monTest.second->multConst_+monTest.second->multConst_);
						if(sc==Scalar(0))
							sum_.erase(s_iter);
						else
						{
							IMonomialComposite * newMc=new IMonomialComposite((*s_iter)->Clone());
							newMc->multConst_=sc;
							sum_.erase(s_iter);
							std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
							assert(insertTest.second);
						}
					}
					else if(s_monTest.first) //only first MC
					{
						Scalar sc(s_monTest.second->multConst_+Scalar(1));
						if(sc==Scalar(0))
							sum_.erase(s_iter);
						else
						{
							IMonomialComposite * newMc=new IMonomialComposite((*s_iter)->Clone());
							newMc->multConst_=sc;
							sum_.erase(s_iter);
							std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
							assert(insertTest.second);
						}
					}
					else if(monTest.first) //only first MC
					{
						Scalar sc(monTest.second->multConst_+Scalar(1));
						if(sc==Scalar(0))
							sum_.erase(s_iter);
						else
						{
							IMonomialComposite * newMc=new IMonomialComposite(arg->Clone());
							newMc->multConst_=sc;
							sum_.erase(s_iter);
							std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
							assert(insertTest.second);
						}
					}
					else //non of them is MC
					{
						IMonomialComposite * newMc=new IMonomialComposite((*s_iter)->Clone());
						newMc->multConst_=Scalar(2);
						sum_.erase(s_iter);
						std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(newMc));
						assert(insertTest.second);
					}
				}
			}
		}
	}
	return *this;
}

ISumComposite & dop::ISumComposite::operator *=(const Scalar arg)
{
	if(arg==Scalar(0))
	{
		addConst_*=arg;
		sum_.clear();
		return *this;
	}
	else if(arg==Scalar(1))
		return *this;
	else
	{
		ISumComposite copyThis(*this);
		addConst_*=arg;
		sum_.clear();
		SumSet::iterator e_iter=copyThis.sum_.end();
		for(SumSet::iterator iter=copyThis.sum_.begin(); iter!=e_iter; ++iter)
		{
			std::pair<bool, IMonomialComposite*> testPair=(*iter)->isMc();
			if(testPair.first)//IF MC, update multConst_
			{
				IMonomialComposite * temp=new IMonomialComposite(*testPair.second);
				temp->operator *=(arg);
				//since copying from sum_, consistent structure assumed, no insert check performed
				std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(temp));
				assert(insertTest.second);
			}
			else //if not MC, make it MC
			{	
				IMonomialComposite * temp=new IMonomialComposite(*iter);
				temp->operator *=(arg);
				//since copying from sum_, consistent structure assumed, no insert check performed
				std::pair<SumSet::iterator,bool> insertTest=sum_.insert(IFunction_ptr(temp));
				assert(insertTest.second);
			}
		}

		return *this;
	}
}

ISumComposite & dop::ISumComposite::operator *=(const IFunction_ptr & arg)
{
	if(arg->isZero())//if arg is zero, then result is zero
	{
		addConst_=Scalar(0);
		sum_.clear();
		return *this;
	}

	std::pair<bool, const ISumComposite *> scalarTest=arg->isScalar();
	if(scalarTest.first)//if arg is scalar, then result*=scalar
	{
		*this*=scalarTest.second->addConst_;
		if(arg->isMc().first)
			delete scalarTest.second;
		return *this;
	}
	else// otherwise, term by term
	{	
		ISumComposite result;			
		std::pair<bool,const ISumComposite *> sumTest=arg->isSc();
		if(sumTest.first)//if sum, each term by each term
		{
			// (sum_i a_i+A)(sum_j b_j+B)=
			// =(sum_i a_i+A)*B [##]
			// +(sum_j (sum_i a_i+A)*b_j ) - [each term handled in the [###] leg of the if]
			const ISumComposite & argRef=*sumTest.second;
			ISumComposite * thisCopy=new ISumComposite(*this);
			thisCopy->operator*=(argRef.addConst_);
			result+=IFunction_ptr(thisCopy); // +(sum_i a_i+A)*B [##]
			// loop for (sum_j (sum_i a_i)*b_j )
			SumSet::const_iterator e_iter=argRef.sum_.end();
			for(SumSet::const_iterator iter=argRef.sum_.begin();iter!=e_iter;++iter)
			{
				ISumComposite * innerThisCopy=new ISumComposite(*this);
				innerThisCopy ->operator*=(*iter); //calling [###]
				result+=IFunction_ptr(innerThisCopy);
			}
			*this=result;
			

			return *this;
		}
		else//if arg is not sum, then each term by arg [###]
		{
			//ISumComposite result;
			//multiplying addConst_
			std::pair<bool, IMonomialComposite*> argMonTest=arg->isMc();
			if(argMonTest.first)//if monomial, use monomial*=scalar
			{
				IMonomialComposite * tempMon=new IMonomialComposite(*argMonTest.second);
				tempMon->operator *=(addConst_);
				result+=IFunction_ptr(tempMon);
			}
			else // if not monomial, create one 
			{
				//ISumComposite result;
				IMonomialComposite * tempMon=new IMonomialComposite(arg);
				tempMon->operator*=(addConst_);
				result+=IFunction_ptr(tempMon);
			}
			//looping through sum_
			SumSet::iterator e_iter=sum_.end();
			for(SumSet::iterator iter=sum_.begin();iter!=e_iter;++iter)
			{
				std::pair<bool, IMonomialComposite*> monTest=(*iter)->isMc();
				if(monTest.first)//if MC, arg+=(MC*arg)
				{
					IMonomialComposite * monTemp=new IMonomialComposite(*monTest.second);
					monTemp->operator*=(arg);
					result+=IFunction_ptr(monTemp);
				}
				else //if not MC, create one
				{
					IMonomialComposite * monTemp=new IMonomialComposite(*iter);
					monTemp->operator*=(arg);
					result+=IFunction_ptr(monTemp);
				}
			}
			*this=result;
			return *this;
		}

	}
	
}




IFunction_ptr dop::ISumComposite::Diff(DiffIndex arg)
{
	ISumComposite * result=new ISumComposite;
	SumSet::const_iterator e_iter=sum_.end();
	for(SumSet::const_iterator iter=sum_.begin(); iter!=e_iter; ++iter)
	{
		result->operator+=((*iter)->Diff(arg));
	}
	return IFunction_ptr(result);
}


Scalar dop::ISumComposite::operator()(const Vector & arg) const
{
	Scalar result(0);
	SumSet::const_iterator e_iter=sum_.end();
	for(SumSet::const_iterator iter=sum_.begin(); iter!=e_iter;++iter)
		result+=((*iter)->operator()(arg));
	result+=addConst_;
	return result;
}

std::pair<bool,IFunction_ptr> dop::ISumComposite::isSingleTerm() 
{
	if(addConst_==Scalar(0) && sum_.size()==1)
	{
		return std::pair<bool, IFunction_ptr>(true, (*sum_.begin())->Clone());
	}
	else
	{
		ISumComposite * result=0;
		return std::pair<bool, IFunction_ptr>(false, IFunction_ptr(result));
	}
}


//**************************************************************
//**************************************************************
// IMonomialComposite
//**************************************************************
//**************************************************************

dop::IMonomialComposite::IMonomialComposite(Scalar arg) : multConst_(Scalar(1))
{}

dop::IMonomialComposite::IMonomialComposite(const IFunction_ptr & Iarg, 
												Exponent Earg) : multConst_(Scalar(1))
{
	if(Iarg->isZero())
		multConst_=Scalar(0);
	else
	{
		std::pair<bool,  ISumComposite *> scalarTest=Iarg->isScalar();
		std::pair<bool,  IMonomialComposite *> testPair=Iarg->isMc();
		if(scalarTest.first)
		{
			multConst_=scalarTest.second->GetAddConst();
			if(Iarg->isMc().first)
				delete scalarTest.second;
		}
		else
		{
			if(testPair.first)
			{
				multConst_=testPair.second->multConst_;
				MonomialMap::const_iterator e_iter=testPair.second->monomial_.end();
				for(MonomialMap::const_iterator iter=testPair.second->monomial_.begin(); iter!=e_iter;++iter)
				{
					Exponent ex=Earg*iter->second;
					if(ex!=Exponent(0))
						monomial_.insert(MonomialPair(iter->first->Clone(),ex));
				}
			}
			else
				monomial_.insert(MonomialPair(Iarg->Clone(),Earg));
		}
		//if(testPair.first)
		//	delete scalarTest.second;
	}
}

dop::IMonomialComposite::IMonomialComposite(const IMonomialComposite & Iarg, 
												Exponent Earg) : multConst_(Scalar(1))
{
	if(Iarg.isZero())
		multConst_=Scalar(0);
	else
	{
		if(Iarg.monomial_.empty())
		{
			multConst_=Iarg.multConst_;
		}
		else
		{
			multConst_=Iarg.multConst_;
			MonomialMap::const_iterator e_iter=Iarg.monomial_.end();
			for(MonomialMap::const_iterator iter=Iarg.monomial_.begin(); iter!=e_iter;++iter)
			{
				Exponent ex=Earg*iter->second;
				if(ex!=Exponent(0))
					monomial_.insert(MonomialPair(iter->first->Clone(),ex));
			}
		}
	}
}

IFunction_ptr dop::IMonomialComposite::Clone()
{
	IMonomialComposite * result=new IMonomialComposite(*this);
	return IFunction_ptr(result);

}


IMonomialComposite & dop::IMonomialComposite::operator=(const IMonomialComposite & arg)
{
	multConst_=arg.multConst_;
	monomial_.clear();
	MonomialMap::const_iterator e_iter=arg.monomial_.end();
	for(MonomialMap::const_iterator iter=arg.monomial_.begin(); iter!=e_iter; ++iter)
		monomial_.insert(MonomialPair(iter->first->Clone(),iter->second));	
	return *this;
}

std::pair<bool, ISumComposite *> dop::IMonomialComposite::isScalar() {	
		if(monomial_.empty())
			return std::pair<bool, ISumComposite *>(true, new ISumComposite(multConst_));
		else
			return std::pair<bool, ISumComposite *>(false,nullptr_t());
}

bool dop::IMonomialComposite::isZero() const
{
	return multConst_==Scalar(0);
}

IMonomialComposite & dop::IMonomialComposite::operator*=(const Scalar arg)
{
	multConst_*=arg;
	return *this;
}

IMonomialComposite & dop::IMonomialComposite::operator*=(const IFunction_ptr & arg)
{
	//arg MC or non SC other assumed
	assert(!arg->isSc().first);

	std::pair<bool, const IMonomialComposite *> testPair=arg->isMc();
	//if MC merge, otherwise insert
	if(testPair.first) //if arg is MC, then "merge"
	{
		multConst_*=testPair.second->multConst_;
		MonomialMap::const_iterator e_iter=testPair.second->monomial_.end();
		for(MonomialMap::const_iterator iter=testPair.second->monomial_.begin(); iter!=e_iter; ++iter)
		{
			InsertIntoMonomial(monomial_,iter->first,iter->second);
		}

	}
	else //if arg is not MC, then "insert"
	{
		InsertIntoMonomial(monomial_,arg);
	}
	return *this;
}

std::ostream&  dop::IMonomialComposite::Print(std::ostream & os) const
{
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	os<< " <<<MonComp_start ";	
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	MonomialMap::const_iterator e_iter=monomial_.end();
	for(MonomialMap::const_iterator iter=monomial_.begin(); iter!=e_iter;++iter)
	{
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		iter->first->Print(os);
		os<< " ^ " << iter->second << " * " ;
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>		
	}
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	os<<  multConst_; 
	os<< " MonComp_finish>>> "	;
	//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	return os;
}

IMonomialComposite & dop::IMonomialComposite::pow(Exponent Earg)
{
	if(Earg==Exponent(1))
		return *this;
	else if(Earg==Exponent(0))
	{
		multConst_=Scalar(1);
		monomial_.clear();
		return *this;
	}
	else 
	{
		multConst_=std::pow(multConst_,Earg);
		MonomialMap::iterator iter=monomial_.begin();
		while(iter!=monomial_.end())
		{
			MonomialPair newPair(iter->first, iter->second*Earg);
			monomial_.erase(iter++); 
			monomial_.insert(newPair); //inserted to the previous place, iter already passed on
		}

		return *this;
	}
}


IFunction_ptr dop::IMonomialComposite::Diff(DiffIndex arg)
{
	std::pair<bool, ISumComposite *> scalarTest=isScalar();
	if( scalarTest.first)
	{
		if(isMc().first)
			delete scalarTest.second;
		return IFunction_ptr(new ISumComposite);
	}

	ISumComposite * result=new ISumComposite;
	MonomialMap::const_iterator e_iter=monomial_.end();
	for(MonomialMap::const_iterator iter=monomial_.begin(); iter!=e_iter; ++iter)
	{
		IMonomialComposite * mon=new IMonomialComposite;
		for(MonomialMap::const_iterator inner_iter=monomial_.begin(); inner_iter!=e_iter; ++inner_iter)	
		{
			//prod_i a_i^b_i -> (prod_{i!=j}a_i^b_i)*b_j a_j^(b_j-1)*a_j'
			if(iter!=inner_iter) //(prod_{i!=j}a_i^b_i)
				mon->operator*=(IFunction_ptr(new IMonomialComposite(inner_iter->first,inner_iter->second)));
			else
			{
				if(inner_iter->second!=Exponent(1))//*b_j a_j^(b_j-1)
				{
					mon->operator*=(IFunction_ptr(new IMonomialComposite(inner_iter->first,inner_iter->second-Exponent(1))));
					mon->operator*=(inner_iter->second);
				}
				//*a_j'
				mon->operator*=(IFunction_ptr(new IMonomialComposite(inner_iter->first->Diff(arg),Exponent(1))));
			}					
		}
		result->operator+=(IFunction_ptr(mon));
	}
	result->operator*=(multConst_);
	return IFunction_ptr(result);
}


Scalar dop::IMonomialComposite::operator()(const Vector & arg) const
{
	Scalar result(1);
	MonomialMap::const_iterator e_iter=monomial_.end();
	for(MonomialMap::const_iterator iter=monomial_.begin(); iter!=e_iter;++iter)
		result*=std::pow(iter->first->operator()(arg),iter->second);
	result*=multConst_;
	return result;
}

//**************************************************************
//**************************************************************
// ORDERING
//**************************************************************
//**************************************************************
bool dop::aux::SumCompLess::operator()(const IFunction_ptr & arg1,const IFunction_ptr & arg2) const
{
	std::pair<bool, ISumComposite*> scalarTest=arg1->isScalar();
	if(scalarTest.first)
	{	
		if(arg1->isMc().first)
			delete scalarTest.second;
		return true;
	}
	else if(arg1->isMc().first || arg2->isMc().first)
		return MonomialCompare().operator ()(arg1,arg2);
	else 			
		return arg1.get()<arg2.get();
}


bool dop::aux::MonomialCompare::operator()(const IFunction_ptr & arg1,const IFunction_ptr & arg2) const
{
	//checking if the lexicographical order of two monomials (up to scalar mult)
	//at least one term is monomial
	//the other could be non-monomial (must be non sum)
		typedef IMonomialComposite::MonomialMap MonomialMap;
		std::pair<bool, const IMonomialComposite *> testPair1=arg1->isMc(); 
		std::pair<bool, const IMonomialComposite *> testPair2=arg2->isMc();
		if(testPair1.first && testPair2.first) //IF BOTH MONOMIAL
		{
			const MonomialMap & monomial1=testPair1.second->monomial_;
			const MonomialMap & monomial2=testPair2.second->monomial_;
			MonomialMap::const_iterator iter1=monomial1.begin();
			MonomialMap::const_iterator iter2=monomial2.begin();
			MonomialMap::const_iterator e_iter1=monomial1.end();
			MonomialMap::const_iterator e_iter2=monomial2.end();
			size_t count(0);
			while(iter1!=e_iter1 && iter2!=e_iter2)//lexicographical kind of comparison
			{
				if(iter1->first.get()>iter2->first.get()) 
					return false;
				else if(iter1->first.get()<iter2->first.get()) 
					return true;
				
				if(iter1->first.get()==iter2->first.get() && iter1->second>iter2->second)
					return false;
				else if(iter1->first.get()==iter2->first.get() && iter1->second<iter2->second)
					return true;
				//else if(iter1->first.get()==iter2->first.get() && iter1->second==iter2->second)

				++count;
				++iter1;
				++iter2;
			}
			return (count==monomial1.size()) && (count<monomial2.size());
		}
		else if(testPair1.first ) //arg1 must be monomial
		{
			//check if arg1 = scalar * arg2
			MonomialMap::const_iterator citer=testPair1.second->monomial_.begin();
			if(testPair1.second->monomial_.size()==1 )//&& citer->second==Scalar(1))
				return citer->first.get()<arg2.get();
			else
				return false;
		}
		else //arg2 must be monomial
		{
			//check if arg2 = scalar * arg1
			MonomialMap::const_iterator citer=testPair2.second->monomial_.begin();
			if(testPair2.second->monomial_.size()==1 )//&& citer->second==Scalar(1))
				return arg1.get()<citer->first.get();
			else
				return false;
		}
	
}

bool dop::aux::MonomialCompLess::operator()(const IFunction_ptr & arg1,const IFunction_ptr & arg2) const
{
	return arg1.get()<arg2.get();
}


//**************************************************************
//**************************************************************
// OTHER HELPER
//**************************************************************
//**************************************************************

void InsertIntoMonomial(dop::IMonomialComposite::MonomialMap & monomial_, const IFunction_ptr &arg, Exponent exarg)
{
	dop::IMonomialComposite::MonomialMap::iterator iter=monomial_.find(arg);
	if(iter==monomial_.end())//not in, then insert
		monomial_.insert(dop::IMonomialComposite::MonomialPair(arg->Clone(),exarg));
	else
	{
		Exponent ex=iter->second+exarg;
		if(ex==0)
			monomial_.erase(iter);
		else
			iter->second=ex;
	}
}

void dop::aux::SingleDiffMap::Insert(IFunction_ptr & arg1, DiffIndex index, IFunction_ptr & arg2)
{	
		DiffMap & dmap=Instance();
		DiffMap::iterator iter=dmap.find(arg1);
		if(iter==dmap.end())
		{
			IndexIFunMap imap;
			imap.insert(IndexIFunPair(index,arg2));
			dmap.insert(DiffPair(arg1,imap));
		}
		else
		{
			IndexIFunMap & imap=iter->second;
			IndexIFunMap::iterator iiter=imap.find(index);
			if(iiter==imap.end())
			{
				imap.insert(IndexIFunPair(index,arg2));
			}
			else
			{
				iiter->second=arg2;
			}
		}
}

IFunction_ptr dop::aux::SingleDiffMap::Find(IFunction_ptr & arg1, DiffIndex index)
{
		DiffMap & dmap=Instance();
		DiffMap::iterator iter=dmap.find(arg1);
		if(iter==dmap.end())
			return IFunction_ptr(new ISumComposite);
		else
		{	
			IndexIFunMap & imap=iter->second;
			IndexIFunMap::iterator iiter=imap.find(index);
			if(iiter==imap.end())
				return IFunction_ptr(new ISumComposite);
			else
				return iiter->second;
		}
}

