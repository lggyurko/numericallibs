//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved. 
#pragma once

#ifndef _SOLUTIONOBJECT_H__
#define _SOLUTIONOBJECT_H__

#include "DifferentialOperator.hpp"

namespace dop
{

typedef std::pair<DifferentialOperator, Scalar> WAlg;
typedef std::vector<WAlg> LieSupport;



/** \class SolutionObjectCub
	\brief Implementation of pure cubature steps.

	Templated with numerical ODE solver (NS) and terminal condition (TC). This way SolutionObjectCub itself 
	(or other variants) can be used as terminal condition, so different PDE numerical methods can be combined. 
	Assumptions:
	- the instance of NS has a member function with signature
	  void operator()(const DifferentialOperator & VF, const Vector & vecIn, Vector & vecOut, Scalar scale);
	- the instance of TC has a member function with signature
	  void operator() (const Vector & vecIn, Vector & vecOut);
	*/
template<class NS, class TC>
class SolutionObjectCub 
{
public:
	/** Constructor
	  * @param numericalSolverArg sets the reference to the numerical ODE solver
	  * @param lieSupportArg sets the reference to the vector field valued random variable
	  * @param terminalConditionArg sets the reference to the terminal condition
	  * @param gammaArg
	  * @param TArg etc.
	  * @param nArg
	  * @param kArg
	  * @param dimArg
	  */
	SolutionObjectCub(NS & numericalSolverArg, LieSupport & lieSupportArg, TC & terminalConditionArg,
					  double gammaArg, double TArg, size_t nArg, size_t kArg, size_t dimArg);
	
	/** The actual functionality.
	  * @param vecIn point to evaluate terminal condition at
	  * @param vecOut result written into 
	  */
	void operator() (const Vector & vecIn, Vector & vecOut);

private:
	NS & numericalSolver_; //!< ODE numerical solver, reference
	LieSupport & lieSupport_; //!< vector field valued random variable, reference
	TC & terminalCondition_; //!< terminal condition
	double gamma_; //!< parameter for uneven partition
	double T_; //!< time to maturity (i.e. time to terminal condition)
	size_t n_; //!<  number of steps
	size_t k_; //!<  index of actual level
	double stepLength_; //!<  step length of the actual level
	size_t dim_; //!< dimension of the output, stored for efficiency reasons
};


/** \fn inline double SetLenght(Scalar , Scalar , size_t , size_t )
    \brief Auxiliary function, used for setting time steps of the uneven partitions, ref.: Kusuoka.
*/
inline double SetLenght(Scalar , Scalar , size_t , size_t );

//*********************************************************************
//*********************************************************************
// IMPLEMENTATIONS
//*********************************************************************
//*********************************************************************
template<class NS, class TC>
SolutionObjectCub<NS, TC>::SolutionObjectCub(NS & numericalSolverArg, LieSupport & lieSupportArg, 
										TC & terminalConditionArg,
										double gammaArg, double TArg, size_t nArg, size_t kArg, size_t dimArg)
				: numericalSolver_(numericalSolverArg), lieSupport_(lieSupportArg),
					terminalCondition_(terminalConditionArg), gamma_(gammaArg), T_(TArg),
					n_(nArg), k_(kArg), dim_(dimArg)
{
	stepLength_=SetLenght(gamma_, T_,n_,k_+1)-SetLenght(gamma_, T_,n_,k_);
}

template<class NS, class TC>
void SolutionObjectCub<NS,TC>::operator() (const Vector & vecIn, Vector & vecOut)
{
	vecOut*=Scalar(0);
	if(n_>k_)
	{
		Vector newVec(vecIn.size());
		Vector resVec(dim_);
		for(size_t i=0; i<lieSupport_.size(); ++i)
		{
			//work out point at the next level
			DifferentialOperator & VF=lieSupport_[i].first;
			numericalSolver_(VF,vecIn,newVec,stepLength_);
					
			//create node in the new points at the next level
			SolutionObjectCub<NS,TC> node(numericalSolver_, lieSupport_, terminalCondition_, gamma_, T_, n_, k_+1, dim_);
			
			//run from node and save results
			node(newVec,resVec);
			vecOut+=resVec*lieSupport_[i].second;
		}
	}
	else
	{
		terminalCondition_(vecIn,vecOut);
	}
	
}




inline Scalar SetLenght(Scalar gamma, Scalar T, size_t n, size_t k)
{
	return T*(1.0 - std::pow(1.0 - ((Scalar) k /(Scalar) n), gamma));

}








}
#endif //_SOLUTIONOBJECT_H__