//Copyright (c) Lajos Gergely Gyurko, 2009-2013. All rights reserved.
#pragma once

#ifndef _DIFFERENTIAL_OPERATOR_H____
#define _DIFFERENTIAL_OPERATOR_H____

#include "Function.hpp"
#include <map>
#include <set>

namespace dop
{
/** \class MultiIndex
	\brief MultiIndex is a model of differentiation indices.

	The class is implemented as a restricted multiset. I.e. the we assume that
	it does not matter in what order the coefficient functions are differentiated.
	*/
class MultiIndex
{
public:
	typedef std::multiset<DiffIndex> MISet;
	typedef MISet::const_iterator const_iterator;

	explicit MultiIndex(const DiffIndex arg);
	MultiIndex & Insert(const DiffIndex arg);
	size_t size() const;
	const_iterator begin() const;
	const_iterator end() const;
	void Append(const MultiIndex & arg);
	bool operator<(const size_t) const;
	bool operator<(const MultiIndex &) const;
	std::ostream&  Print(std::ostream & os) const;

private:
	MISet multiIndex_;
};

MultiIndex Append(const MultiIndex & arg1, const MultiIndex & arg2);

inline std::ostream & operator<<(std::ostream& os, const MultiIndex & mIndex)
{
	return mIndex.Print(os);
}

typedef std::pair<Degree, MultiIndex> IndexPair; //!< first term is degree, second is a multi-index

/** \class IndexPairComp
	\brief Function object for ordering IndexPair instances
*/
class IndexPairComp : public std::binary_function<const IndexPair,const IndexPair,bool>
{
public:
	bool operator()(const IndexPair & arg1,const IndexPair & arg2) const;
};

//**************************************************************
//**************************************************************
// DIFFERENTIAL OPERATOR
//**************************************************************
//**************************************************************

/** \class DifferentialOperator
	\brief Model of vector fields and higher order differential operators on R^n.

	The core of the class is a map of pairs of  degrees and multi-indices to Functions.
	The degree of the drift term must be 1, and the terms corresponding to dB terms
	must be half.
	*/
class DifferentialOperator
{
public:
	typedef std::map<IndexPair,Function, IndexPairComp> DOMap;
	typedef std::pair<IndexPair,Function> DOPair;

	/** Constructor.
	 *	Constructs an empty operator.
	 * @param sizeArg sets size (i.e. n)
	 * @param maxDegreeArg set the max degree, i.e. truncation level
	 * (in the sense of rough paths of inhomogeneous degree smoothness)
	 */
	DifferentialOperator(const size_t sizeArg, const Degree maxDegreeArg);

	/** Constructor.
	 *	Constructs first order differential operator: f(x)d_i
	 * @param sizeArg sets size (i.e. n)
	 * @param maxDegreeArg set the max degree i.e. truncation level
	 * @param degreeArg sets degree of the single term
	 * (in the sense of rough paths of inhomogeneous degree smoothness)
	 * @param diffIndexArg sets the differentiation index i
	 * @param FunctionArg sets the Function f(x)
	 */
	DifferentialOperator(const size_t sizeArg, Degree maxDegreeArg,
					Degree degreeArg, DiffIndex diffIndexArg, const Function & FunctionArg);

	/** Constructor.
	 *	Constructs first order differential operator: f(x)d_i
	 * @param sizeArg sets size (i.e. n)
	 * @param maxDegreeArg set the max degree i.e. truncation level
	 * @param degreeArg sets degree of the single term
	 * (in the sense of rough paths of inhomogeneous degree smoothness)
	 * @param multiIndexArg sets the differentiation index i
	 * @param FunctionArg sets the Function f(x)
	 */
	DifferentialOperator(const size_t sizeArg, Degree maxDegreeArg,
					Degree degreeArg, MultiIndex multiIndexArg, const Function & FunctionArg);

	/** Get function for size_.
	  */
	size_t size() const;

	/** Inserting a first order term.
	  * @param degree sets the degree of the new term
	  * @param diffIndexArg is the differential index of the new term
	  * @param FunctionArg sets the coefficient function
	  * @return if the insert was successful
	  */
	bool Insert(Degree degree, const DiffIndex diffIndexArg, const Function & FunctionArg);

	/** Inserting a higher order term.
	  * @param degree sets the degree of the new term
	  * @param multIndex is the differential index of the new term
	  * @param FunctionArg sets the coefficient function
	  * @return if the insert was successful
	  */
	bool Insert(Degree degree, const MultiIndex & multIndex, const Function & FunctionArg);

	/** operator() - the main functionality.
	  * evaluates DiffOp(Id(arg)), i.e. the first order terms only.
	  * @param InVec is the argument
	  * @param OutVec the result is written into
	  * @param Scalar lambda is the rescaling parameter
	  */
	void operator()(const Vector & InVec, Vector & OutVec, Scalar lambda=Scalar(1)) const;


	DifferentialOperator & operator+=(const DOPair &);
	DifferentialOperator & operator-=(const DOPair &);
	DifferentialOperator & operator+=(const DifferentialOperator &);
	DifferentialOperator & operator-=(const DifferentialOperator &);
	DifferentialOperator & operator*=( Scalar );
	DifferentialOperator & operator*=(const Function &  );

	/** Operator composition NOT multiplication.
	 *  Key functionality for DiffereintialOperator operations.
	 */
	DifferentialOperator & operator*=(const DifferentialOperator &);
	DifferentialOperator & exp(size_t);

	/** Dropping terms with multi-indices longer than arg.
	  * @param arg is the truncation level
	  */
	DifferentialOperator & TruncateByLength(size_t arg);

	/** Dropping terms with degree higher than arg
	  * @param arg is the truncation level
	  */
	DifferentialOperator & TruncateByDegree(Degree arg);


	/**
	* Outputing the function.
	* Auxiliary member, mainly for testing purposes.
	*/
	std::ostream&  Print(std::ostream & os) const;

private:
	DOMap dop_; //!< map contaioner of multi-indices and coefficient functions.
	size_t size_; //!< dimensionality.
	Degree maxDegree_; //!< max degree - truncation level.

};

inline std::ostream & operator<<(std::ostream& os, const DifferentialOperator & diffOp)
{
	return diffOp.Print(os);
}

DifferentialOperator operator+(const DifferentialOperator & arg1,const DifferentialOperator & arg2);
DifferentialOperator operator-(const DifferentialOperator & arg1,const DifferentialOperator & arg2);
DifferentialOperator operator*(const DifferentialOperator & arg1,const Scalar & arg2);
DifferentialOperator operator*(const Scalar & arg2, const DifferentialOperator & arg1);

/** \fn DifferentialOperator operator*(const DifferentialOperator & arg1,const DifferentialOperator & arg2);
  * \brief returns agr1 applied on arg2 i.e.: arg1(arg2)
  */
DifferentialOperator operator*(const DifferentialOperator & arg1,const DifferentialOperator & arg2);

/** \fn DifferentialOperator LieBracket(const DifferentialOperator & arg1,const DifferentialOperator & arg2);
  * \brief returns the Lie bracket [arg1,arg2]=arg1*arg2-arg2*arg1
  */
DifferentialOperator LieBracket(const DifferentialOperator & arg1,const DifferentialOperator & arg2);


}
#endif //_DIFFERENTIAL_OPERATOR_H____

