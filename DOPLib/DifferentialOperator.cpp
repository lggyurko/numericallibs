//Copyright (c) Lajos Gergely Gyurko, 2009. All rights reserved.

#include "DifferentialOperator.hpp"

using namespace dop;

dop::MultiIndex::MultiIndex(const DiffIndex arg)
{
	multiIndex_.insert(arg);
}

MultiIndex & dop::MultiIndex::Insert(const DiffIndex arg)
{
	multiIndex_.insert(arg);
	return *this;
}

size_t dop::MultiIndex::size() const
{
	return multiIndex_.size();
}

dop::MultiIndex::const_iterator dop::MultiIndex::begin() const
{
	return multiIndex_.begin();
}

dop::MultiIndex::const_iterator dop::MultiIndex::end() const
{
	return multiIndex_.end();
}

void dop::MultiIndex::Append(const MultiIndex & arg)
{
	multiIndex_.insert(arg.begin(), arg.end());
}

MultiIndex dop::Append(const MultiIndex & arg1, const MultiIndex & arg2)
{
	MultiIndex result(arg1);
	result.Append(arg2);
	return result;
}

bool dop::MultiIndex::operator <(const size_t arg) const
{
	const_iterator e_iter=multiIndex_.end();
	const_iterator iter=multiIndex_.begin();
	while(iter!=e_iter)
	{
		if(*iter>=arg)
			return false;
		++iter;
	}
	return true;
}

bool dop::MultiIndex::operator<(const MultiIndex & arg) const
{
	if(size()<arg.size())
		return true;
	else if(size()>arg.size())
		return false;

	MultiIndex::const_iterator e_iter1=end();
	MultiIndex::const_iterator iter1=begin();
	MultiIndex::const_iterator iter2=arg.begin();
	while(iter1!=e_iter1)
	{
		if(*iter1<*iter2)
			return true;
		else if(*iter1>*iter2)
			return false;
		else
		{
			++iter1;
			++iter2;
		}
	}
	return false;


}

std::ostream&  dop::MultiIndex::Print(std::ostream & os) const
{
	os<< "<<<MULTI_INDEX_start " << std::endl;
	MISet::const_iterator e_iter=multiIndex_.end();
	MISet::const_iterator iter=multiIndex_.begin();
	for(; iter!=e_iter; ++iter)
	{
		os<< *iter << " " ;
	}
	os<< std::endl;
	os<< "<<<MULTI_INDEX_ends " << std::endl;
	return os;
}

bool dop::IndexPairComp::operator ()(const dop::IndexPair &arg1, const dop::IndexPair &arg2) const
{
	if(arg1.second<arg2.second)
		return true;
	else if(arg2.second<arg1.second)
		return false;

	if(arg1.first<arg2.first)
		return true;
	else
		return false;

}

//**************************************************************
//**************************************************************
// DIFFERENTIALOPERATOR
//**************************************************************
//**************************************************************

dop::DifferentialOperator::DifferentialOperator(const size_t sizeArg, const Degree degreeArg)
		: size_(sizeArg), maxDegree_(degreeArg)
{}

dop::DifferentialOperator::DifferentialOperator(const size_t sizeArg, Degree maxDegreeArg,
					Degree degree, DiffIndex diffIndexArg, const Function & FunctionArg)
		: size_(sizeArg), maxDegree_(maxDegreeArg)
{
	if(diffIndexArg<size_ && degree<=maxDegree_) //check if fits to size, and FunctionArg is allowed
		dop_.insert(DOPair(IndexPair(degree,MultiIndex(diffIndexArg)),FunctionArg));
}

dop::DifferentialOperator::DifferentialOperator(const size_t sizeArg, Degree maxDegreeArg,
					Degree degree, MultiIndex multiIndexArg, const Function & FunctionArg)
		: size_(sizeArg), maxDegree_(maxDegreeArg)
{
	if(multiIndexArg<size_ && degree<=maxDegree_) //check if fits to size, and FunctionArg is allowed
		dop_.insert(DOPair(IndexPair(degree,multiIndexArg),FunctionArg));
}

size_t dop::DifferentialOperator::size() const
{
	return size_;
}
bool dop::DifferentialOperator::Insert(Degree degree, const DiffIndex diffIndexArg,
									   const Function & FunctionArg)
{
	if(diffIndexArg<size_ && degree<=maxDegree_) // check if fits to dimension
	{
		//check if term not yet exists
		std::pair<DOMap::iterator, bool> testInsert=
			dop_.insert(DOPair(IndexPair(degree,MultiIndex(diffIndexArg)),FunctionArg));
		return true && testInsert.second;
	}
	else
		return false;
}

bool dop::DifferentialOperator::Insert(Degree degree, const MultiIndex & multIndex,
									   const Function & FunctionArg)
{
	if(multIndex<size_ && degree<=maxDegree_) // check if fits to dimension
	{
		//check if term not yet exists
		std::pair<DOMap::iterator, bool> testInsert=dop_.insert(DOPair(IndexPair(degree,multIndex),FunctionArg));
		return true && testInsert.second;
	}
	else
		return false;
}

void dop::DifferentialOperator::operator()(const Vector & InVec, Vector & OutVec, Scalar lambda) const
{
	//assert(OutVec.size()==size_);
	OutVec.resize(size_); // should check size, however it is a performance overhead
	OutVec*=Scalar(0);
	if(!dop_.empty())
	{
		DOMap::const_iterator iter=dop_.begin();
		DOMap::const_iterator e_iter=dop_.end();
		DiffIndex indexLength=iter->first.second.size();
		while(indexLength==1)
		{
			const Function & fn(iter->second);
			const Degree & deg=iter->first.first;
			OutVec(*(iter->first.second.begin()))+=fn(InVec)*std::pow(lambda,deg);
			++iter;
			if(iter==e_iter)
				break;
			else
				indexLength=iter->first.second.size();
		}
	}

}

DifferentialOperator & dop::DifferentialOperator::operator+=(const DOPair & arg)
{
	assert(arg.first.second<size_);
	if(arg.first.first<=maxDegree_)
	{
		DOMap::iterator s_iter=dop_.find(arg.first);
		if(s_iter==dop_.end())
		{
			if(!arg.second.isZero())
				dop_.insert(arg);
		}
		else
		{
			Function fn=s_iter->second+arg.second;
			if(!fn.isZero())
				s_iter->second=fn;
			else
				dop_.erase(s_iter);
		}
	}
	return *this;
}


DifferentialOperator & dop::DifferentialOperator::operator-=(const DOPair & arg)
{
	assert(arg.first.second<size_);
	if(arg.first.first<=maxDegree_)
	{
		DOMap::iterator s_iter=dop_.find(arg.first);
		if(s_iter==dop_.end())
		{
			Function fn=arg.second*(-Scalar(1));
			dop_.insert(std::make_pair(arg.first,fn));
		}
		else
		{
			Function fn=s_iter->second-arg.second;
			if(!fn.isZero())
				s_iter->second=fn;
			else
				dop_.erase(s_iter);
		}
	}
	return *this;
}

DifferentialOperator & dop::DifferentialOperator::operator+=(const DifferentialOperator & arg)
{
	assert(size_==arg.size_ && maxDegree_==arg.maxDegree_); //check compatibility
	DOMap::const_iterator iter=arg.dop_.begin();
	DOMap::const_iterator e_iter=arg.dop_.end();
	for(;iter!=e_iter;++iter)
	{
		*this+=*iter;
	}
	return *this;
}

DifferentialOperator & dop::DifferentialOperator::operator-=(const DifferentialOperator & arg)
{
	assert(size_==arg.size_ && maxDegree_==arg.maxDegree_); //check compatibility
	DOMap::const_iterator iter=arg.dop_.begin();
	DOMap::const_iterator e_iter=arg.dop_.end();
	for(;iter!=e_iter;++iter)
	{
		*this-=*iter;
	}
	return *this;
}

DifferentialOperator & dop::DifferentialOperator::operator*=(Scalar arg)
{
	if(arg==0)
	{
		dop_.clear();
		return *this;
	}
	else if(arg!=Scalar(1))
	{
		DOMap::iterator iter=dop_.begin();
		DOMap::iterator e_iter=dop_.end();
		for(; iter!=e_iter; ++iter)
		{
			iter->second*=arg;
		}
	}
	return *this;
}

DifferentialOperator & dop::DifferentialOperator::operator*=(const Function & arg)
{
	//degree of the Function's in dop_ is left unchanged
	if(arg.isZero())
	{
		dop_.clear();
		return *this;
	}
	else
	{
		DOMap::iterator iter=dop_.begin();
		DOMap::iterator e_iter=dop_.end();
		for(; iter!=e_iter; ++iter)
		{
			iter->second*=arg;
		}
	}
	return *this;
}

//operator composition (NOT multiplication)
// arg is applied on this: arg(this)
DifferentialOperator & dop::DifferentialOperator::operator*=(const DifferentialOperator & arg)
{
	assert(size_==arg.size_); //check compatibility
	DifferentialOperator copyThis(*this);
	dop_.clear();
	DOMap::const_iterator arg_iter=arg.dop_.begin();
	DOMap::const_iterator arg_e_iter=arg.dop_.end();
	for(; arg_iter!=arg_e_iter; ++arg_iter)//looping through the terms in arg
	{
		const MultiIndex & arg_mIndex(arg_iter->first.second);
		const Degree & arg_degree(arg_iter->first.first);
		const Function & arg_function(arg_iter->second);

		DOMap::iterator s_iter=copyThis.dop_.begin();
		DOMap::iterator s_e_iter=copyThis.dop_.end();
		for(; s_iter!=s_e_iter; ++s_iter)// given a term in arg, looping through terms in this
		{
			Degree newDegree=s_iter->first.first+arg_degree;
			if(newDegree<=maxDegree_)
			{
				MultiIndex::const_iterator indexIter=arg_mIndex.begin();
				MultiIndex::const_iterator e_indexIter=arg_mIndex.end();

				DifferentialOperator tempOp(size_, maxDegree_);
				tempOp.Insert(s_iter->first.first, s_iter->first.second, s_iter->second);//bD_I
				//want to work out D_J(bD_I)=D_j1(D_j2(...(D_jk(b_D_I)
				for(; indexIter!=e_indexIter; ++indexIter)//looping through arg_mIndex=J
				{
					DifferentialOperator innerTempOp(tempOp);//copy of tempOp
					tempOp.dop_.clear(); //D_jk of innerTempOp is written into tempOp
					DOMap::iterator inner_e_iter=innerTempOp.dop_.end();
					DOMap::iterator inner_iter=innerTempOp.dop_.begin();

					//sum_I D_jk(bD_I)=sum_I bD_(jk*I)+(D_jk(b))D_I
					for(; inner_iter!=inner_e_iter;++inner_iter)//looping thorugh I
					{
						//bD_(jk*I)
						DifferentialOperator firstTerm(size_,maxDegree_,newDegree,
							MultiIndex(inner_iter->first.second).Insert(*indexIter),
							inner_iter->second);
						//(D_ij b)D_I
						DifferentialOperator secondTerm(size_,maxDegree_,newDegree,
							inner_iter->first.second,
							Diff(inner_iter->second,*indexIter));
						tempOp+=firstTerm;
						tempOp+=secondTerm;
					}
				}
				//now tempOp=sum_I D_J(bD_I)
				tempOp*=arg_function; 	//a sum_I D_J(bD_I)
				*this+=tempOp; //adding effect of a D_I
			}
		}
	}
	return *this;
}

DifferentialOperator & dop::DifferentialOperator::TruncateByLength(size_t arg)
{
	DOMap::iterator iter=dop_.begin();
	while(iter!=dop_.end())
	{
		if(iter->first.second.size()>arg)
			dop_.erase(iter++);
		else
			++iter;
	}
	return *this;
}

DifferentialOperator & dop::DifferentialOperator::TruncateByDegree(Degree arg)
{
	DOMap::iterator iter=dop_.begin();
	while(iter!=dop_.end())
	{
		if(iter->first.first>arg)
			dop_.erase(iter++);
		else
			++iter;
	}
	return *this;
}


std::ostream&  dop::DifferentialOperator::Print(std::ostream & os) const
{
	os<< std::endl;
	os<< "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << std::endl;
	os<< "<<<DIFFERENTIAL OPERATOR_start " << std::endl;
	DOMap::const_iterator e_iter=dop_.end();
	DOMap::const_iterator iter=dop_.begin();
	for(; iter!=e_iter;++iter)
	{
		os<< "degree: " << iter->first.first << std::endl;
		os<< iter->first.second ;
		os<< iter->second;
	}
	os<< " DIFFERENTIAL OPERATOR_finish>>> " << std::endl;
	os<< ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << std::endl;
	os<< std::endl;
	return os;

}

DifferentialOperator dop::operator+(const DifferentialOperator & arg1,const DifferentialOperator & arg2)
{
	DifferentialOperator result(arg1);
	return result+=arg2;

}

DifferentialOperator dop::operator-(const DifferentialOperator & arg1,const DifferentialOperator & arg2)
{
	DifferentialOperator result(arg1);
	return result-=arg2;

}

DifferentialOperator dop::operator*(const DifferentialOperator & arg1,const DifferentialOperator & arg2)
{
	DifferentialOperator result(arg2);
	return result*=arg1;
}

DifferentialOperator dop::operator*(const DifferentialOperator & arg1,const Scalar & arg2)
{
	DifferentialOperator result(arg1);
	return result*=arg2;

}

DifferentialOperator dop::operator*(const Scalar & arg1, const DifferentialOperator & arg2)
{
	DifferentialOperator result(arg2);
	return result*=arg1;
}


DifferentialOperator dop::LieBracket(const DifferentialOperator & arg1,const DifferentialOperator & arg2)
{
	return arg1*arg2-arg2*arg1;
}

//DifferentialOperator & dop::DifferentialOperator::exp(size_t);
